# Driver Assistant #

This is a concept of mobile application which is focused on detection and recognition of road traffic signs in order to provide information to a driver about driving conditions.

### Application ###

[Google Play](https://play.google.com/store/apps/details?id=chernyshov.yuriy.driverassistant)

### What is this repository for? ###

* This repository is created in purpose of sharing personal experience with objects detection and recognition in the field of road traffic signs.

### Wiki ###

Navigate to the [Wiki](https://bitbucket.org/ChernyshovYuriy/driver-assistant-android/wiki/Home) to be familiar with application.

### Who do I talk to? ###

Project owner and admin - Yurii Chernyshov

E-mail : chernyshov.yuriy@gmail.com

LinkedIn: https://www.linkedin.com/in/yurii-chernyshov/