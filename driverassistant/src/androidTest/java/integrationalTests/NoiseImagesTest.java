package integrationalTests;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.test.ActivityInstrumentationTestCase2;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import chernyshov.yuriy.driverassistant.core.ResourceImage;
import chernyshov.yuriy.driverassistant.test.R;
import chernyshov.yuriy.driverassistant.utils.AppLogger;
import chernyshov.yuriy.driverassistant.ui.activities.MainActivity;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 26/03/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */

public final class NoiseImagesTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Context mContext;

    public NoiseImagesTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        mContext = getInstrumentation().getContext();

        OpenCVLoader.initDebug();
    }

    public void testNoiseImages() {
        for (final ResourceImage resourceImage : getNoiseImages()) {
            final Bitmap bitmap = BitmapFactory.decodeStream(
                    mContext.getResources().openRawResource(resourceImage.getId())
            );
            final Mat mat = new Mat();
            Utils.bitmapToMat(bitmap, mat);

        }
    }

    private List<ResourceImage> getNoiseImages() {
        int id;
        String name;
        final List<ResourceImage> result = new ArrayList<>();
        final Field[] rawClassFields = R.raw.class.getFields();
        for (final Field field : rawClassFields) {
            name = field.getName();
            try {
                id = field.getInt(field);
            } catch (final IllegalAccessException e) {
                AppLogger.e("Can not get resource id:" + e.getMessage());
                continue;
            }

            if (name.startsWith("noise_img")) {
                result.add(new ResourceImage(name, id, -1));
            }
        }
        return result;
    }
}
