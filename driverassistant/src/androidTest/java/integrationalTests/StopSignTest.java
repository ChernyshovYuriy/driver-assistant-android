package integrationalTests;

import android.content.Context;
import android.test.ActivityInstrumentationTestCase2;

import org.opencv.android.OpenCVLoader;

import chernyshov.yuriy.driverassistant.ui.activities.MainActivity;

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 26/03/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */

public final class StopSignTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private Context mContext;

    public StopSignTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        mContext = getInstrumentation().getContext();

        OpenCVLoader.initDebug();

    }
}
