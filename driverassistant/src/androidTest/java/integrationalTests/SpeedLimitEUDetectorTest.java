package integrationalTests;

import com.yuriy.detection.DetectedShape;

import java.util.concurrent.CountDownLatch;

import chernyshov.yuriy.driverassistant.core.recognition.CompositeSignDetection;
import chernyshov.yuriy.driverassistant.core.recognition.SpeedRecognitionEU;
import chernyshov.yuriy.driverassistant.test.R;
import chernyshov.yuriy.driverassistant.utils.AppLogger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 26.11.14
 * Time: 16:42
 */
public final class SpeedLimitEUDetectorTest extends AbstractDetectorTest {

    private String mDetectedResult;

    public SpeedLimitEUDetectorTest() {
        super();
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        countDownLatch = new CountDownLatch(1);
        mCompositeSpeedDetector = new CompositeSignDetection(
                new SpeedRecognitionEU()
        );

        mOcrDetectorProcessor.setListener(
                (roadSign, detectedText) -> {
                    AppLogger.d("Detected:" + detectedText);
                    mDetectedResult = detectedText;
                    countDownLatch.countDown();
                }
        );
    }

    /**
     * 5 km/h
     */

    public void testRecognize5kmhFromRealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_5_ideal);
        assertThat(mDetectedResult, is("5"));
    }

    /**
     * 10 km/h
     */

    public void testRecognize10kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_10_ideal);
        assertThat(mDetectedResult, is("10"));
    }

    /**
     * 15 km/h
     */

    public void testRecognize15kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_15_ideal);
        assertThat(mDetectedResult, is("15"));
    }

    /**
     * 20 km/h
     */

    public void testRecognize20kmhFromRealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_01);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromRealImage_01() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_02);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromRealImage_03() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_03);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromRealImage_04() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_04);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromRealImage_05() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_05);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromRealImage_06() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_06);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromRealImage_07() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_real_07);
        assertThat(mDetectedResult, is("20"));
    }

    public void testRecognize20kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_20_ideal);
        assertThat(mDetectedResult, is("20"));
    }

    /**
     * 30 km/h
     */

    public void testRecognize30kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_30_ideal);
        assertThat(mDetectedResult, is("30"));
    }

    /**
     * 40 km/h
     */

    public void testRecognize40kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_40_ideal);
        assertThat(mDetectedResult, is("40"));
    }

    /**
     * 50 km/h
     */

    public void testRecognize50kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_ideal);
        assertThat(mDetectedResult, is("50"));
    }

    public void testRecognize50kmhFromRealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_real_01);
        assertThat(mDetectedResult, is("50"));
    }

    public void testRecognize50kmhFromRealImageV02() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_real_02);
        assertThat(mDetectedResult, is("50"));
    }

    public void testRecognize50kmhFromRealImageV03() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_real_03);
        assertThat(mDetectedResult, is("50"));
    }

    public void testRecognize50kmhFromRealImageV04() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_real_04);
        assertThat(mDetectedResult, is("50"));
    }

    public void testRecognize50kmhFromRealImageV05() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_real_05);
        assertThat(mDetectedResult, is("50"));
    }

    public void testRecognize50kmhFromRealImageV06() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_50_real_06);
        assertThat(mDetectedResult, is("50"));
    }

    /**
     * 60 km/h
     */

    public void testRecognize60kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_ideal);
        assertThat(mDetectedResult, is("60"));
    }

    public void testRecognize60kmhFromRealImageV01() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_real_01);
        assertThat(mDetectedResult, is("60"));
    }

    public void testRecognize60kmhFromRealImageV02() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_real_02);
        assertThat(mDetectedResult, is("60"));
    }

    public void testRecognize60kmhFromRealImageV03() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_real_03);
        assertThat(mDetectedResult, is("60"));
    }

    public void testRecognize60kmhFromRealImageV04() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_real_04);
        assertThat(mDetectedResult, is("60"));
    }

    public void testRecognize60kmhFromRealImageV05() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_real_05);
        assertThat(mDetectedResult, is("60"));
    }

    public void testRecognize60kmhFromRealImageV06() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_60_real_06);
        assertThat(mDetectedResult, is("60"));
    }

    /**
     * 70 km/h
     */

    public void testRecognize70kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_70_ideal);
        assertThat(mDetectedResult, is("70"));
    }

    /**
     * 80 km/h
     */

    public void testRecognize80kmhFromIdealImage() throws Exception {
        initTestData(DetectedShape.SHAPE.ELLIPSE, R.raw.img_80_ideal);
        assertThat(mDetectedResult, is("80"));
    }
}
