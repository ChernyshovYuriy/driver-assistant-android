package integrationalTests;

import android.graphics.Bitmap;
import android.os.SystemClock;
import android.test.ActivityInstrumentationTestCase2;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextRecognizer;
import com.yuriy.detection.DetectedShape;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import chernyshov.yuriy.driverassistant.core.OCRDetectorProcessor;
import chernyshov.yuriy.driverassistant.core.recognition.CompositeSignDetection;
import chernyshov.yuriy.driverassistant.utils.FileUtils;
import chernyshov.yuriy.driverassistant.ui.activities.MainActivity;

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 26.11.14
 * Time: 16:42
 */
public class AbstractDetectorTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private static final int LATCH_TIMEOUT = 1;
    CompositeSignDetection mCompositeSpeedDetector;
    private TextRecognizer mTextRecognizer;
    CountDownLatch countDownLatch;
    OCRDetectorProcessor mOcrDetectorProcessor;

    public AbstractDetectorTest() {
        super(MainActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        OpenCVLoader.initDebug();

        mTextRecognizer = new TextRecognizer.Builder(
                getInstrumentation().getTargetContext()
        ).build();
        mOcrDetectorProcessor = new OCRDetectorProcessor();
        mTextRecognizer.setProcessor(mOcrDetectorProcessor);
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();

        if (mTextRecognizer != null) {
            mTextRecognizer.release();
        }
    }

    /**
     * This method create test data and pass it to the recognizer.
     *
     * @param resourceId Id of the raw resource that represents a road sign.
     * @throws Exception
     */
    void initTestData(final DetectedShape.SHAPE shape,
                      final int resourceId) throws Exception {
        final Mat mat = Utils.loadResource(getInstrumentation().getContext(), resourceId);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2GRAY);
        mCompositeSpeedDetector.detectShape(
                mat,
                shape,
                (roadSign, detectionResult) -> {
                    int widthD = detectionResult.getWidth();
                    int heightD = detectionResult.getHeight();
                    // Create bitmap
                    Bitmap bitmap = Bitmap.createBitmap(
                            widthD, heightD, Bitmap.Config.RGB_565
                    );

                    // TODO: Refactor
                    final int[] data = new int[detectionResult.getData().length];
                    for (int i = 0; i < data.length; i++) {
                        data[i] = detectionResult.getData()[i];
                    }

                    // Set the pixels
                    bitmap.setPixels(
                            data,
                            0, widthD, 0, 0, widthD, heightD
                    );

                    FileUtils.saveBitmapToFile(bitmap, "TEST");

                    final int rotation = 0;
                    final long pendingTime = SystemClock.elapsedRealtime();
                    final int pendingFrameId = roadSign.ordinal();

                    // TODO : Refactor to use setImageData

                    final Frame frame = new Frame.Builder()
                            .setBitmap(bitmap)
                            .setId(pendingFrameId)
                            .setTimestampMillis(pendingTime)
                            .setRotation(rotation)
                            .build();

                    mTextRecognizer.receiveFrame(frame);
                });

        countDownLatch.await(LATCH_TIMEOUT, TimeUnit.SECONDS);
    }
}
