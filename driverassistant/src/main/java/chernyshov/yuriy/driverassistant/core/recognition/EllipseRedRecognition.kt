/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core.recognition

import android.content.Context
import chernyshov.yuriy.driverassistant.R.raw
import chernyshov.yuriy.driverassistant.core.DetectedListener
import chernyshov.yuriy.driverassistant.core.detection.DetectionCallback
import chernyshov.yuriy.driverassistant.core.detection.DetectionResult
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.FileUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.opencv.core.Mat
import java.util.*
import java.util.concurrent.atomic.*

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 01.06.14
 * Time: 19:10
 */
class EllipseRedRecognition(context: Context) : DetectedListener {

    private val mSignRecognition: SignRecognition
    private val mIsInit: AtomicBoolean

    override fun detectShape(mat: Mat, detectionCallback: DetectionCallback) {
        if (!mIsInit.get()) {
            return
        }
        val start = System.currentTimeMillis().toDouble()
        val id = mSignRecognition.recognize(mat)
        AppLogger.d(
            "Red ellipse " + id + " recognized in "
                    + (System.currentTimeMillis() - start) + " ms"
        )

        // Handle STOP sign case. It handles in separate recognition file but participated
        // in ellipse training. Probably it has sense to exclude it from training.
        if (id == 3) {
            return
        }

        detectionCallback.onComplete(RoadSign.getSign(id), DetectionResult.makeEmptyResult())
    }

    private class FileVO(val mPath: String, val mId: Int)

    companion object {

        private const val PREFIX = "trng_img_"

        private fun getImages(context: Context): MutableList<FileVO> {
            var id: Int
            var name: String
            val result: MutableList<FileVO> = ArrayList()
            val rawClassFields = raw::class.java.fields
            for (field in rawClassFields) {
                name = field.name
                id = try {
                    field.getInt(field)
                } catch (e: IllegalAccessException) {
                    AppLogger.e("Can not get resource id:" + e.message)
                    continue
                }
                if (name.startsWith(PREFIX)) {
                    result.add(
                        FileVO(
                            FileUtils.loadFile(context, id, name).absolutePath,
                            getId(name)
                        )
                    )
                }
            }
            return result
        }

        private fun getId(name: String): Int {
            var arr = name.split(PREFIX.toRegex()).toTypedArray()
            arr = arr[1].split("_".toRegex()).toTypedArray()
            var value = arr[0]
            if (value.startsWith("0")) {
                value = value.replaceFirst("0".toRegex(), "")
            }
            return value.toInt()
        }
    }

    init {
        AppLogger.d("EllipseRedRecognition start train")
        mIsInit = AtomicBoolean(false)
        mIsInit.set(false)
        mSignRecognition = SignRecognition()

        GlobalScope.launch(Dispatchers.IO) {
            val list = getImages(context)
            for (file in list) {
                mSignRecognition.add(file.mPath, file.mId)
            }
            list.clear()
            mSignRecognition.init()
            mIsInit.set(true)
            AppLogger.d("EllipseRedRecognition end init")
        }
    }
}
