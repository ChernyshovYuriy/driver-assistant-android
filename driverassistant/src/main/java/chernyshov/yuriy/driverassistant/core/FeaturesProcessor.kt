/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.SparseIntArray
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.core.detection.RoadSign.Companion.getSign
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.core.MatOfDMatch
import org.opencv.core.MatOfKeyPoint
import org.opencv.core.Size
import org.opencv.features2d.DescriptorMatcher
import org.opencv.features2d.Feature2D
import org.opencv.features2d.ORB
import org.opencv.imgproc.Imgproc
import java.lang.reflect.Field
import java.util.*

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 19/03/17
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 *
 * This class designed in order to provide functionality to train [DescriptorMatcher]
 * and use it to compare input [Mat] with trained data set. This gives a way to
 * determine whether comparing [Mat] represents a known Road Sign or not.
 */
class FeaturesProcessor(private val mListener: FeaturesProcessorListener) {

    interface FeaturesProcessorListener {
        fun onDataTrained()
    }

    private val mMatch = MatOfDMatch()
    private val mMatOfKeyPoint = MatOfKeyPoint()
    private val mDescriptor = Mat()
    private val mMat = Mat()
    private val mDetector: Feature2D
    private val mDescriptorExtractor: Feature2D
    private val mMatcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMINGLUT)
    private val mImgLabels = SparseIntArray()

    /**
     * Compare provided input with trained data set.
     *
     * @param bitmap Input [Bitmap] represented potential Road Sign.
     * @return Label corresponded to the Road Sing
     * (see https://bitbucket.org/ChernyshovYuriy/driver-assistant-dataset/overview)
     * or [.UNKNOWN_RESULT].
     */
    fun compare(bitmap: Bitmap): Int {
        val start = System.currentTimeMillis().toDouble()
        prepareMat(bitmap, mMat)
        mDetector.detect(mMat, mMatOfKeyPoint)
        mDescriptorExtractor.compute(mMat, mMatOfKeyPoint, mDescriptor)
        mMatcher.match(mDescriptor, mMatch)
        val matches = mMatch.toArray()
        for (dMatch in matches) {
            LABELS_COUNT[mImgLabels[dMatch.imgIdx, UNKNOWN_RESULT]]++
        }
        var maxValue = UNKNOWN_RESULT
        var label = UNKNOWN_RESULT
        for ((counter, count) in LABELS_COUNT.withIndex()) {
            if (count > maxValue) {
                maxValue = count
                label = counter
            }
        }
        val confidence = maxValue / (matches.size * 1.0)
        mMat.release()
        mDescriptor.release()
        mMatch.release()
        Arrays.fill(LABELS_COUNT, 0)

        // Do not consider a case when index is not a number.
        if (java.lang.Double.isNaN(label.toDouble())) {
            label = UNKNOWN_RESULT
        }
        // Do not consider a case when confidence is too low.
        if (confidence < 0.70) {
            label = UNKNOWN_RESULT
        }
        // Do not consider a case when there are too few matches found.
        if (matches.size <= 10) {
            label = UNKNOWN_RESULT
        }
        AppLogger.d(
            "Label:" + getSign(label)
                    + " \tconfidence:" + confidence + " (" + maxValue + "/" + matches.size + ")"
                    + " \ttime " + (System.currentTimeMillis() - start) + " ms"
        )
        return label
    }

    /**
     * Train [DescriptorMatcher] with the training data set of the images
     * (see https://bitbucket.org/ChernyshovYuriy/driver-assistant-dataset/overview).
     *
     * @param context        Application context.
     * @param rawClassFields Array of [Field]s of the project's raw resource,
     * this is R.raw.class.getFields().
     */
    fun trainMatcher(context: Context, rawClassFields: Array<Field>) {
        GlobalScope.launch(Dispatchers.IO) { trainMatcherInternal(context, rawClassFields) }
    }

    /**
     * Returns collection of the IDs of the training images locating in the raw resources folder.
     *
     * @param rawClassFields Array of [Field]s of the project's raw resource,
     * this is R.raw.class.getFields().
     * @return Collection of the raw resources IDs used for the training.
     */
    fun getTrainingData(rawClassFields: Array<Field>): List<ResourceImage> {
        var id: Int
        var label: Int
        var name: String
        val result: MutableList<ResourceImage> = ArrayList()
        for (field in rawClassFields) {
            name = field.name
            id = try {
                field.getInt(field)
            } catch (e: IllegalAccessException) {
                AppLogger.e("Can not get resource id:" + e.message)
                continue
            }
            if (name.startsWith(TRAINING_IMG_PREFIX)) {
                AppLogger.i("Training asset: $name id:$id")
                label = getTrainingImageLabel(name)
                result.add(ResourceImage(name, id, label))
            }
        }
        return result
    }

    /**
     * Get a value of the Road Sign Label based on the image file name
     * (see https://bitbucket.org/ChernyshovYuriy/driver-assistant-dataset/overview).
     *
     * @param name Image file name.
     * @return Road Sign Label.
     */
    private fun getTrainingImageLabel(name: String): Int {
        val array = name.split(TRAINING_IMG_SEPARATOR.toRegex()).toTypedArray()
        return array[2].toInt()
    }

    /**
     *
     * @param context
     * @param rawClassFields
     */
    private fun trainMatcherInternal(
        context: Context,
        rawClassFields: Array<Field>
    ) {
        val start = System.currentTimeMillis().toDouble()
        val descriptors: MutableList<Mat> = ArrayList()
        var counter = 0
        val resourceImages = getTrainingData(rawClassFields)
        for (resourceImage in resourceImages) {
            val bitmap = BitmapFactory.decodeStream(
                context.resources.openRawResource(resourceImage.id)
            )
            val mat = Mat()
            prepareMat(bitmap, mat)
            bitmap.recycle()
            val key = counter
            mImgLabels.put(key, resourceImage.label)
            counter++
            val matOfKeyPoint = MatOfKeyPoint()
            val descriptor = Mat()
            mDetector.detect(mat, matOfKeyPoint)
            mDescriptorExtractor.compute(mat, matOfKeyPoint, descriptor)
            mat.release()
            if (descriptor.empty()) {
                AppLogger.w("Descriptor is empty, name:" + resourceImage.name)
                continue
            }

            // For BRIEF, ORB, FREAK, AKAZE etc descriptor have to be CvType.CV_32F
            // For SURF, SIFT, etc descriptor have to be CV_8U
            // Refer https://github.com/opencv/opencv/issues/5937
            //descriptor.convertTo(descriptor, CvType.CV_32F);
            descriptors.add(descriptor)
        }
        mMatcher.add(descriptors)
        mMatcher.train()
        AppLogger.d(
            "Matcher trained by " + counter + " images in "
                    + (System.currentTimeMillis() - start) + " ms"
        )

//        String url = FileUtils.getExternalStorageDir() + "/a_matcher.txt";
//        FileUtils.createFile(url);
//        mMatcher.write(url);
//
//        url = FileUtils.getExternalStorageDir() + "/a_descriptor_extractor.txt";
//        FileUtils.createFile(url);
//        mDescriptorExtractor.write(url);
//
//        url = FileUtils.getExternalStorageDir() + "/a_detector.txt";
//        FileUtils.createFile(url);
//        mDetector.write(url);
        mListener.onDataTrained()
    }

    companion object {
        const val UNKNOWN_RESULT = -1
        private const val TRAINING_IMG_PREFIX = "trng_img_"
        private const val TRAINING_IMG_SEPARATOR = "_"
        private val SIZE = Size(Constants.TRAIN_INPUT_W.toDouble(), Constants.TRAIN_INPUT_H.toDouble())
        private val LABELS_COUNT = IntArray(RoadSign.values().size)

        /**
         * @param bitmap
         * @param mat
         */
        private fun prepareMat(bitmap: Bitmap, mat: Mat) {
            Utils.bitmapToMat(bitmap, mat)
            Imgproc.resize(mat, mat, SIZE)
            Imgproc.cvtColor(mat, mat, Imgproc.COLOR_RGB2GRAY)
        }
    }

    /**
     * Main constructor.
     */
    init {
        val nFeatures = 50
        val scaleFactor = 1.0f
        val nLevels = 5
        val edgeThreshold = 10
        val firstLevel = 0
        val wta_k = 2
        val scoreType = ORB.HARRIS_SCORE
        val patchSize = 10
        val fastThreshold = 20
        // http://docs.opencv.org/3.2.0/db/d95/classcv_1_1ORB.html
        mDescriptorExtractor = ORB.create(
            nFeatures, scaleFactor, nLevels, edgeThreshold, firstLevel, wta_k, scoreType,
            patchSize, fastThreshold
        )
        mDetector = ORB.create(
            nFeatures, scaleFactor, nLevels, edgeThreshold, firstLevel, wta_k, scoreType,
            patchSize, fastThreshold
        )
        AppLogger.d("Descr extractor:$mDescriptorExtractor")
        AppLogger.d("Feature detector:$mDetector")
        AppLogger.d("FlannBasedMatcher:$mMatcher")
    }
}
