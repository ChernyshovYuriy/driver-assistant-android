/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core.recognition

import android.content.Context
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 7/25/15
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * This class provides a functionality of detection the Speed Limit Canada Sign
 * from the detected area.
 */
class SpeedRecognitionCanada(context: Context) : AbstractTextRecognition(
    context,
    RoadSign.SPEED_LIMIT_CANADA, SIZE, SCALE_X, SCALE_Y, CLIP_LIMIT, THRESHOLD_TYPE
) {

    companion object {
        private val SIZE = Size(120.0, 120.0)
        private const val SCALE_X = 10
        private const val SCALE_Y = 26
        private const val CLIP_LIMIT = 10
        private const val THRESHOLD_TYPE = Imgproc.THRESH_OTSU
    }
}
