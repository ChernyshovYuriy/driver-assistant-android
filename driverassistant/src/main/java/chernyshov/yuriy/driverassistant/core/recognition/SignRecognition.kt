/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core.recognition

import org.opencv.core.Mat

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 16/04/18
 * E-Mail: chernyshov.yuriy@gmail.com
 */
class SignRecognition {

    private var mNativeObj: Long

    fun init() {
        nativeInit(mNativeObj)
    }

    fun add(filePath: String, index: Int) {
        nativeAdd(mNativeObj, filePath, index)
    }

    fun release() {
        nativeDestroyObject(mNativeObj)
        mNativeObj = 0
    }

    fun recognize(mat: Mat): Int {
        return nativeRecognize(mNativeObj, mat.nativeObj)
    }

    external fun nativeCreateObject(): Long
    external fun nativeDestroyObject(thiz: Long)
    external fun nativeInit(thiz: Long)
    external fun nativeAdd(thiz: Long, filePath: String, index: Int)
    external fun nativeRecognize(thiz: Long, inputImage: Long): Int

    init {
        mNativeObj = nativeCreateObject()
    }
}
