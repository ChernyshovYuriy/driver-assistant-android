/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core

import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.Detector.Detections
import com.google.android.gms.vision.text.TextBlock

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 9/8/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
class OCRDetectorProcessor : Detector.Processor<TextBlock> {

    private var mListener: OCRDetectorProcessorListener? = null

    fun setListener(value: OCRDetectorProcessorListener?) {
        mListener = value
    }

    override fun receiveDetections(detections: Detections<TextBlock>) {
        val metadata = detections.frameMetadata ?: return
        val items = detections.detectedItems
        var textBlock: TextBlock
        val size = items.size()
        val id = metadata.id
        for (i in 0 until size) {
            textBlock = items.valueAt(i)
            if (mListener != null) {
                mListener!!.onDetect(getRoadSign(id), textBlock.value)
            }
        }
    }

    override fun release() {}

    companion object {

        private fun getRoadSign(value: Int): RoadSign {
            for (roadSign in RoadSign.values()) {
                if (roadSign.ordinal == value) {
                    return roadSign
                }
            }
            return RoadSign.UNKNOWN
        }
    }
}
