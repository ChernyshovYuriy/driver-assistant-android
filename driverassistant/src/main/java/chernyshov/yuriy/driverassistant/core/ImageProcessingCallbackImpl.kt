/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core

import android.content.Context
import chernyshov.yuriy.driverassistant.core.detection.DetectedShape
import chernyshov.yuriy.driverassistant.core.detection.DetectedShape.SHAPE
import chernyshov.yuriy.driverassistant.core.detection.DetectedShape.TYPE
import chernyshov.yuriy.driverassistant.core.detection.DetectionCallback
import chernyshov.yuriy.driverassistant.core.recognition.EllipseRedRecognition
import chernyshov.yuriy.driverassistant.core.recognition.SpeedRecognitionCanada
import chernyshov.yuriy.driverassistant.core.recognition.SpeedRecognitionEU
import chernyshov.yuriy.driverassistant.core.recognition.StopSignRecognition
import chernyshov.yuriy.driverassistant.utils.AppLogger.d
import chernyshov.yuriy.driverassistant.utils.FileUtils.saveMatToFile

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 01.06.14
 * Time: 17:30
 */
class ImageProcessingCallbackImpl(private val mContext: Context) : ImageProcessingCallbackBase {

    private val mSpeedRecognitionEU: DetectedListener
    private val mSpeedRecognitionCanada: DetectedListener
    private val mEllipseRedRecognition: DetectedListener
    private val mStopSignRecognition: DetectedListener
    private var mDetectionCallback: DetectionCallback? = null
    private var mSaveDetectedShape = false

    init {
        mSpeedRecognitionEU = SpeedRecognitionEU(mContext)
        mSpeedRecognitionCanada = SpeedRecognitionCanada(mContext)
        mEllipseRedRecognition = EllipseRedRecognition(mContext)
        mStopSignRecognition = StopSignRecognition(mContext)
    }

    fun setDetectionCallback(value: DetectionCallback) {
        d("$CLASS_NAME set detection callback $value")
        mDetectionCallback = value
    }

    override fun onDetect(detectedShape: DetectedShape) {
        d("$CLASS_NAME Recognize shape: $detectedShape")
        val dirName: String
        val matClone = detectedShape.mat.clone()
        when (detectedShape.shape) {
            SHAPE.ELLIPSE -> {
                dirName = "detected_ellipse"
                when (detectedShape.type) {
                    TYPE.SPEED_LIMIT -> mSpeedRecognitionEU.detectShape(matClone, mDetectionCallback!!)
                    TYPE.ELLIPSE_RED -> mEllipseRedRecognition.detectShape(matClone, mDetectionCallback!!)
                    TYPE.STOP -> {
                    }
                }
            }
            SHAPE.RECTANGLE -> {
                dirName = "detected_rect"
                mSpeedRecognitionCanada.detectShape(matClone, mDetectionCallback!!)
            }
            SHAPE.OCTAGON -> {
                // For simplicity, currently, Stop sign is only one octagon.
                dirName = "detected_octa"
                mStopSignRecognition.detectShape(matClone, mDetectionCallback!!)
            }
            else -> dirName = "unknown"
        }
        if (mSaveDetectedShape) {
            saveMatToFile(mContext, matClone, dirName)
        }
        matClone.release()
    }

    fun setSaveDetectedShape(value: Boolean) {
        mSaveDetectedShape = value
    }

    companion object {
        private val CLASS_NAME = ImageProcessingCallbackImpl::class.java.simpleName
    }
}
