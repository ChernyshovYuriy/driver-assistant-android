/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core

import org.opencv.core.Mat
import org.opencv.core.MatOfRect

class DetectionBasedTracker(cascadeName: String, minObjectSize: Int) {

    fun setMinObjectSize(size: Int) {
        nativeSetObjectSize(mNativeObj, size)
    }

    fun detect(imageGray: Mat, objects: MatOfRect, scaleFactor: Float) {
        nativeDetect(mNativeObj, imageGray.nativeObj, objects.nativeObj, scaleFactor)
    }

    fun release() {
        nativeDestroyObject(mNativeObj)
        mNativeObj = 0
    }

    var mNativeObj: Long

    private external fun nativeCreateObject(cascadeName: String, minObjectSize: Int): Long
    private external fun nativeDestroyObject(thiz: Long)
    private external fun nativeSetObjectSize(thiz: Long, size: Int)
    private external fun nativeDetect(thiz: Long, inputImage: Long, objects: Long, scaleFactor: Float)

    init {
        mNativeObj = nativeCreateObject(cascadeName, minObjectSize)
    }
}
