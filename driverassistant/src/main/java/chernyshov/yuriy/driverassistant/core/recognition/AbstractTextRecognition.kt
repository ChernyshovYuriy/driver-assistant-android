/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core.recognition

import android.content.Context
import chernyshov.yuriy.driverassistant.core.DetectedListener
import chernyshov.yuriy.driverassistant.core.detection.DetectionCallback
import chernyshov.yuriy.driverassistant.core.detection.DetectionResult
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.utils.AppLogger.d
import chernyshov.yuriy.driverassistant.utils.AppLogger.w
import chernyshov.yuriy.driverassistant.utils.FileUtils.arrayToImage
import chernyshov.yuriy.driverassistant.utils.MatUtils.resizeImage
import org.opencv.core.*
import org.opencv.imgproc.Imgproc

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 7/25/15
 * E-Mail: chernyshov.yuriy@gmail.com
 */
abstract class AbstractTextRecognition internal constructor(
    private val mContext: Context,
    private val mRoadSign: RoadSign,
    private val mSize: Size,
    private val mScaleX: Int,
    private val mScaleY: Int,
    private val mClipLimit: Int,
    private val mThresholdType: Int
) : DetectedListener {
    private val mCLAHE = Imgproc.createCLAHE()

    override fun detectShape(mat: Mat, detectionCallback: DetectionCallback) {
        val startTime = System.currentTimeMillis()
        val matResized = resizeImage(mat, mSize)
        val channel = Mat()
        // Extract the L channel
        Core.extractChannel(matResized, channel, 0)
        // apply the CLAHE algorithm to the L channel
        mCLAHE.clipLimit = mClipLimit.toDouble()
        mCLAHE.apply(channel, channel)
        //        clahe.collectGarbage();
        // Merge the the color planes back into an Lab image
        Core.insertChannel(channel, matResized, 0)
        // Temporary Mat not reused, so release from memory.
        channel.release()
        Imgproc.threshold(
            matResized,
            matResized, 0.0, 255.0,
            mThresholdType
        )
        val sizeCropped = matResized.size()
        val x = (sizeCropped.width * mScaleX / 100).toInt()
        val y = (sizeCropped.height * mScaleY / 100).toInt()
        val rectCropped = Rect(
            x, y,
            (sizeCropped.width - x * 2).toInt(),
            (sizeCropped.height - y * 2).toInt()
        )
        val cropped = matResized.submat(rectCropped)
        matResized.release()
        val croppedCopy = Mat(cropped.rows(), cropped.cols(), cropped.type(), WHITE)
        val croppedCopyLarge = Mat(
            (sizeCropped.width + PADDING).toInt(),
            (sizeCropped.height + PADDING * 2).toInt(), cropped.type(),
            WHITE
        )
        val canny = Mat()
        Imgproc.Canny(cropped, canny, 50.0, 200.0)
        val points = ArrayList<MatOfPoint>()
        Imgproc.findContours(
            canny, points, Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE
        )
        canny.release()
        for (point in points) {
            val area = Imgproc.contourArea(point)
            val rect = Imgproc.boundingRect(point)
            //          AppLogger.d("AREA:" + area + " RECT:" + mRect.toString());
            // TODO: Improve
            if (rect.height >= cropped.rows() * 0.9) {
                Imgproc.fillConvexPoly(cropped, point, WHITE)
                continue
            }
            if (rect.width < 10 && rect.x > cropped.cols() / 2) {
                Imgproc.fillConvexPoly(cropped, point, WHITE)
                continue
            }

//            if (mRect.x <= 1 || mRect. y <= 1) {
//                Imgproc.fillConvexPoly(mCropped, point, WHITE);
//                continue;
//            }
            if (area > 200) {
                cropped.submat(rect).copyTo(croppedCopy.submat(rect))
                continue
            }
            if (rect.width in 5..10 && rect.height >= 50) {
//                Imgproc.fillConvexPoly(mCropped, point, WHITE);
                cropped.submat(rect).copyTo(croppedCopy.submat(rect))
                continue
            }
            if (rect.height < 20) {
                Imgproc.fillConvexPoly(cropped, point, WHITE)
            } else {
                if (rect.width <= 5) {
                    Imgproc.fillConvexPoly(cropped, point, WHITE)
                    continue
                }
                cropped.submat(rect).copyTo(croppedCopy.submat(rect))
            }
        }
        val bytes = ByteArray((croppedCopyLarge.total() * croppedCopyLarge.channels()).toInt())
        rectCropped.x = (croppedCopyLarge.cols() - rectCropped.width) / 2
        rectCropped.y = (croppedCopyLarge.rows() - rectCropped.height) / 2
        if (croppedCopyLarge.cols() <= 0 || croppedCopyLarge.rows() <= 0 //                || 0 <= rectCropped.x || 0 <= rectCropped.width
            //                || rectCropped.x + rectCropped.width < mCroppedCopyLarge.cols()
            //                || 0 <= rectCropped.y || 0 <= rectCropped.height
            || rectCropped.y + rectCropped.height >= croppedCopyLarge.rows()
        ) {
            w("    FailedData:$rectCropped $croppedCopyLarge")
            return
        }
        val croppedCopySubmat = croppedCopyLarge.submat(rectCropped)
        croppedCopy.copyTo(croppedCopySubmat)
        croppedCopyLarge[0, 0, bytes]

//        Mat kernel = new Mat(new Size(1, 1), CvType.CV_8UC1, new Scalar(255));
//        Imgproc.morphologyEx(mCroppedCopyLarge, mCroppedCopyLarge, Imgproc.MORPH_CLOSE, kernel);
//        Imgproc.medianBlur(mCroppedCopyLarge, mCroppedCopyLarge, 1);
        d("Process time (speed limit):" + (System.currentTimeMillis() - startTime) + " ms")
        val detectionResult = DetectionResult(
            bytes, croppedCopyLarge.cols(), croppedCopyLarge.rows()
        )
        arrayToImage(
            mContext,
            bytes,
            croppedCopyLarge.cols(),
            croppedCopyLarge.rows(),
            "pre-ocr"
        )
        cropped.release()
        croppedCopy.release()
        croppedCopyLarge.release()
        croppedCopySubmat.release()
        detectionCallback.onComplete(mRoadSign, detectionResult)
    }

    companion object {
        private val WHITE = Scalar(255.0, 255.0, 255.0)
        private const val PADDING = 3
    }
}
