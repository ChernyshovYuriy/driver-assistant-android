/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core

import chernyshov.yuriy.driverassistant.core.detection.DetectedShape
import chernyshov.yuriy.driverassistant.utils.AppLogger
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 2/17/14
 * Time: 11:19 PM
 */
class VideoFrameProcessor(
    private val mCascadeProcessors: Array<AbstractCascadeProcessor>,
    private val mImageProcessingCallback: ImageProcessingCallbackBase
) : VideoDataObserver {

    private var mLastCallBackCounter = 0.0
    private val mIsInit = AtomicBoolean(false)
    @Volatile
    private var mWidth = 0
    @Volatile
    private var mHeight = 0
    private val mSize = Size()
    private val mCLAHE = Imgproc.createCLAHE()
    private var mScope = CoroutineScope(Dispatchers.IO)

    init {
        mSize.width = WIDTH
        mCLAHE.clipLimit = 4.0
    }

    /**
     * Initialize Camera Preview callback.
     *
     * @param width  Width of the image.
     * @param height Height of the image.
     */
    fun init(width: Int, height: Int) {
        if (mIsInit.get()) {
            AppLogger.i("$CLASS_NAME VFP already initialized")
            return
        }
        mWidth = width
        mHeight = height
        AppLogger.i("$CLASS_NAME init with ${mWidth}x$mHeight")
        mSize.height = mHeight * (WIDTH / mWidth)
        for (processor in mCascadeProcessors) {
            processor.updateSize(mWidth, mHeight)
        }
        mIsInit.set(true)
    }

    fun deInit() {
        AppLogger.i("$CLASS_NAME de-init")
        mIsInit.set(false)
    }

    val isInit: Boolean
        get() = mIsInit.get()

    /**
     *
     */
    fun stop() {
        AppLogger.i("$CLASS_NAME stop")
        mScope.cancel("Cancel on stop")
        deInit()
    }

    override fun update(videoData: ByteArray) {
        // http://developer.android.com/reference/android/graphics/ImageFormat.html
        // camera.getParameters().getPreviewFormat()
        // 17        - NV21 (YCrCb)
        // 842094169 - YUV

        // Assume initialization not done.
        if (!mIsInit.get()) {
            return
        }
        val currentTimeMillis = System.currentTimeMillis()
        if (currentTimeMillis - mLastCallBackCounter <= SKIP_PROCESSING_INTERVAL) {
            return
        }
        val frame = Mat(mHeight, mWidth, CvType.CV_8UC1)
        if (videoData.size % CvType.channels(frame.type()) != 0) {
            return
        }
        mLastCallBackCounter = currentTimeMillis.toDouble()
        frame.put(0, 0, videoData)
        val channel = Mat()
        // Extract the L channel
        Core.extractChannel(frame, channel, 0)
        // apply the CLAHE algorithm to the L channel
        mCLAHE.apply(channel, channel)
        // Merge the the color planes back into an Lab image
        Core.insertChannel(channel, frame, 0)
        // Temporary Mat not reused, so release from memory.
        channel.release()
        frame.put(0, 0, videoData)
        mScope.launch {
            val start = System.currentTimeMillis().toDouble()
            if (!mIsInit.get()) {
                return@launch
            }
            val mat: Mat
            synchronized(MONITOR) {
                mat = Mat(frame.height(), frame.width(), CvType.CV_8UC1)
                Imgproc.resize(frame, mat, mSize)
                Imgproc.equalizeHist(mat, mat)
            }
            var cascadeResults: Array<CascadeResult?>
            for (processor in mCascadeProcessors) {
                if (!mIsInit.get()) {
                    return@launch
                }
                cascadeResults = processor.detect(mat)
                for ((c, result) in cascadeResults.withIndex()) {
                    mImageProcessingCallback.onDetect(
                        DetectedShape(
                            processor.associatedShape,
                            processor.associatedType,
                            result!!.mat
                        )
                    )
                    result.release()
                    cascadeResults[c] = null
                }
            }
            AppLogger.d("Frame processed in ${System.currentTimeMillis() - start} ms")
        }
    }

    companion object {

        private val CLASS_NAME = VideoFrameProcessor::class.java.simpleName

        /**
         * Time in millisecond between processing frames.
         */
        private const val SKIP_PROCESSING_INTERVAL = 1000 / 20
        private const val WIDTH = 640.0
        private val MONITOR = Any()
    }
}
