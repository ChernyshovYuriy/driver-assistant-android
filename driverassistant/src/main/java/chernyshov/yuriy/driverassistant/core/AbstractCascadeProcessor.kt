/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core

import android.content.Context
import chernyshov.yuriy.driverassistant.core.detection.DetectedShape
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.FileUtils
import org.opencv.core.Mat
import org.opencv.core.MatOfRect
import org.opencv.core.Rect

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 23/12/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
abstract class AbstractCascadeProcessor internal constructor(
    private val mResourceId: Int,
    private val mFileName: String,
    val associatedShape: DetectedShape.SHAPE,
    val associatedType: DetectedShape.TYPE
) {

    private var mNativeDetector: DetectionBasedTracker? = null
    private val mObjects = MatOfRect()
    private var mScaleFactor = 1.1f

    /**
     * Initialize cascade classifier.
     *
     * @param context Context of application.
     */
    fun init(context: Context?) {
        val cascadeFile = FileUtils.loadFile(context!!, mResourceId, mFileName)
        mNativeDetector = DetectionBasedTracker(cascadeFile.absolutePath, 0)
        mNativeDetector!!.setMinObjectSize(OBJECT_SIZE)
        AppLogger.d("Cascade processor fully initialized, " + mNativeDetector!!.mNativeObj)
    }

    fun updateSize(width: Int, height: Int) {
        //TODO: Not implemented.
    }

    /**
     * Detect object from the provided frame data.
     *
     * @param mat [Mat] represented single frame.
     * @return Array of the detected Mat objects or an empty array.
     */
    @Synchronized
    fun detect(mat: Mat): Array<CascadeResult?> {
        mNativeDetector?.detect(mat, mObjects, mScaleFactor)
        val objectsArray = mObjects.toArray()
        val numObjects = objectsArray.size
        AppLogger.d("Cascade detected " + numObjects + " objects, " + javaClass.simpleName)
        val result = arrayOfNulls<CascadeResult>(numObjects)
        var rect: Rect?
        for (i in 0 until numObjects) {
            rect = objectsArray[i]
            result[i] = CascadeResult(Mat(mat, rect))
            objectsArray[i] = null
        }
        mObjects.release()
        //        mMat.release();
        return result
    }

    companion object {
        private const val OBJECT_SIZE = 20
    }
}
