/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.core.detection

/**
 * Created with IntelliJ IDEA.
 * User: Yuriy Chernyshov
 * Date: 7/5/15
 * Time: 8:19 PM
 *
 */
enum class RoadSign(val id: Int) {
    /**
     *
     */
    NO_LEFT_TURN(0),

    /**
     *
     */
    NO_RIGHT_TURN(1),

    /**
     *
     */
    NO_U_TURN(2),

    /**
     *
     */
    STOP(3),

    /**
     *
     */
    NO_PARKING(4),

    /**
     *
     */
    NO_STOPPING(5),

    /**
     *
     */
    SPEED_LIMIT_EU(6),

    /**
     *
     */
    SPEED_LIMIT_CANADA(7),

    /**
     *
     */
    NO_LEFT_RIGHT_TURN(8),

    /**
     *
     */
    NO_STRAIGHT(9),

    /**
     *
     */
    UNKNOWN(-1);

    companion object {

        @JvmStatic
        fun getSign(id: Int): RoadSign {
            for ((counter, roadSign) in values().withIndex()) {
                if (counter == id) {
                    return roadSign
                }
            }
            return UNKNOWN
        }
    }
}
