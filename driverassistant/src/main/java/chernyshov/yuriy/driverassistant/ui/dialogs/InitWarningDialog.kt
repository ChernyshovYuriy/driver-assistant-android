/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.ui.activities.MainActivity

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 08.06.14
 * Time: 16:20
 *
 * [InitWarningDialog] is a Dialog to show
 * important information before use this application.
 */
class InitWarningDialog : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Set dialog's title
        dialog!!.setTitle(activity?.getString(R.string.init_warning_title))

        // Inflate view
        val view = inflater.inflate(R.layout.init_warning_layout, container, false)

        // process positive button click
        val okButton = view.findViewById<Button>(R.id.init_warning_ok_btn_view)
        okButton.setOnClickListener {
            (activity as MainActivity?)!!.setInitWarningDialogShown()
            dismiss()
        }
        return view
    }

    companion object {
        /**
         * Create a new instance of [InitWarningDialog]
         */
        fun newInstance(): InitWarningDialog {
            return InitWarningDialog()
        }
    }
}
