/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.activities

import android.content.*
import android.os.Bundle
import android.os.IBinder
import android.preference.CheckBoxPreference
import android.preference.PreferenceActivity
import android.preference.PreferenceFragment
import android.speech.tts.TextToSpeech
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.services.AppService
import chernyshov.yuriy.driverassistant.services.AppService.AppServiceBinder
import chernyshov.yuriy.driverassistant.ui.SafeToast
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.Constants
import chernyshov.yuriy.driverassistant.utils.IntentBuilder
import java.lang.ref.WeakReference

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 09.06.14
 * Time: 22:56
 */
class SettingsActivity : PreferenceActivity() {
    /**
     * Application Service.
     */
    private var mAppService: AppService? = null
    private var mIsTTSEnabledPreference: CheckBoxPreference? = null
    private val mServiceConnection: ServiceConnection = ServiceConnectionImpl(this)
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppLogger.i("$CLASS_NAME On Create")
        bindService(
                AppService.createIntent(applicationContext),
                mServiceConnection,
                BIND_AUTO_CREATE
        )
    }

    public override fun onPause() {
        super.onPause()
        AppLogger.i("$CLASS_NAME On Pause")
        if (mAppService == null) {
            return
        }
        AppLogger.i("$CLASS_NAME Unbound PreferenceManagerService")
        unbindService(mServiceConnection)
        mAppService = null
    }

    override fun onBuildHeaders(target: List<Header>) {
        loadHeadersFromResource(R.xml.preference_headers, target)
    }

    override fun isValidFragment(fragmentName: String): Boolean {
        AppLogger.d("$CLASS_NAME valid fragment:$fragmentName")
        return CameraPreferences::class.java.name == fragmentName || TTSPreferences::class.java.name == fragmentName || GeneralPreferences::class.java.name == fragmentName || SpeedLimitPreferences::class.java.name == fragmentName || RecognitionPreferences::class.java.name == fragmentName
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int,
                                  data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.ActivityResultCode.TTS_CHECK_CODE) {
            var isTTSEngineAvailable = false
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                AppLogger.i("TTS Engine is available")
                // TTS Engine installed on the device.
                isTTSEngineAvailable = true
            } else {
                AppLogger.w("TTS Engine is NOT available")
                // Missing TTS Engine, install it.
                val intent = IntentBuilder.createInstallTTSIntent()
                if (intent.resolveActivity(packageManager) != null) {
                    startActivity(intent)
                } else {
                    AppLogger.e("Can not install Text-To-Speech engine")
                }
            }
            if (mIsTTSEnabledPreference == null) {
                return
            }
            mIsTTSEnabledPreference!!.isEnabled = isTTSEngineAvailable
            if (mAppService == null) {
                return
            }
            mAppService!!.isTTSEngineEnabled = isTTSEngineAvailable && mIsTTSEnabledPreference!!.isChecked
        }
    }

    private fun initializeTTSPreferences(isTTSEnabledPreference: CheckBoxPreference) {
        mIsTTSEnabledPreference = isTTSEnabledPreference
        try {
            startActivityForResult(
                    IntentBuilder.createCheckTTSIntent(),
                    Constants.ActivityResultCode.TTS_CHECK_CODE
            )
        } catch (e: ActivityNotFoundException) {
            SafeToast.showToastAnyThread(
                    applicationContext,
                    "Text-To-Speech Library is not available"
            )
        }
    }

    class GeneralPreferences : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.general_preferences)
        }
    }

    class CameraPreferences : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.camera_preferences)
        }
    }

    class RecognitionPreferences : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.recognition_preferences)
        }
    }

    class TTSPreferences : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.tts_preferences)
            (activity as SettingsActivity).initializeTTSPreferences(
                    findPreference(getString(R.string.pref_key_tts_enabled)) as CheckBoxPreference
            )
        }
    }

    class SpeedLimitPreferences : PreferenceFragment() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.speed_limit_preferences)
        }
    }

    /**
     *
     */
    private class ServiceConnectionImpl(reference: SettingsActivity) : ServiceConnection {

        private val mReference = WeakReference(reference)

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            val settingsActivity = mReference.get() ?: return
            settingsActivity.mAppService = (service as AppServiceBinder).service
            AppLogger.i("Bound to PreferenceManagerService")
        }

        override fun onServiceDisconnected(name: ComponentName) {
            AppLogger.w("PreferenceMangerService disconnected unexpectedly")
            val settingsActivity = mReference.get() ?: return
            settingsActivity.mAppService = null
        }

    }

    companion object {
        private val CLASS_NAME = SettingsActivity::class.java.simpleName

        /**
         * Factory method to create an [android.content.Intent].
         *
         * @param context Context of the callee.
         * @return Instance of the [android.content.Intent]
         */
        @JvmStatic
        fun createIntent(context: Context?): Intent {
            return Intent(context, SettingsActivity::class.java)
        }
    }
}