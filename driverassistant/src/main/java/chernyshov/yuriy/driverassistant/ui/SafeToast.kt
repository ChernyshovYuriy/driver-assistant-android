/*
 * Copyright 2017-21 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui

import android.content.Context
import android.widget.Toast
import chernyshov.yuriy.driverassistant.utils.AppUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 06.11.12
 * Time: 15:57
 */
internal object SafeToast {

    @JvmStatic
    fun showToastAnyThread(context: Context?, text: CharSequence?) {
        if (AppUtils.isRunningUIThread) {
            // We are already in UI thread, it's safe to show Toast.
            showToastUIThread(context, text)
        } else {
            // We are NOT in UI thread, so scheduling task.
            GlobalScope.launch(Dispatchers.Main) { showToastUIThread(context, text) }
        }
    }

    private fun showToastUIThread(context: Context?, text: CharSequence?) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show()
    }
}
