/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.dialogs

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.utils.AppUtils

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 07.06.14
 * Time: 20:06
 */
class AboutDialog : DialogFragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        dialog!!.setTitle(activity?.getString(R.string.app_name)
                + "    v." +
                AppUtils.getApplicationVersionName(requireContext())
                + "." + AppUtils.getApplicationVersionCode(requireContext()))
        val view = inflater.inflate(R.layout.about_layout, container, false)
        val authorLink = view.findViewById<TextView>(R.id.about_author_link_view)
        authorLink.setOnClickListener {
            val browse = Intent(Intent.ACTION_VIEW, Uri.parse(AUTHOR_PROFILE_URL))
            startActivity(browse)
        }
        val projectHomeLink = view.findViewById<TextView>(R.id.about_project_link_view)
        projectHomeLink.setOnClickListener {
            val browse = Intent(Intent.ACTION_VIEW, Uri.parse(PROJECT_HOME_URL))
            startActivity(browse)
        }
        val privacyLink = view.findViewById<TextView>(R.id.about_project_privacy_view)
        privacyLink.setOnClickListener {
            val browse = Intent(Intent.ACTION_VIEW, Uri.parse(PRIVACY__URL))
            startActivity(browse)
        }
        return view
    }

    companion object {
        private const val AUTHOR_PROFILE_URL = "http://www.linkedin.com/pub/yuriy-chernyshov/1b/622/270"
        private const val PROJECT_HOME_URL = "https://bitbucket.org/ChernyshovYuriy/driver-assistant"
        private const val PRIVACY__URL = "https://bitbucket.org/ChernyshovYuriy/driver-assistant/wiki/Privacy%20Policy"

        /**
         * Create a new instance of [chernyshov.yuriy.driverassistant.ui.AboutDialog]
         */
        fun newInstance(): AboutDialog {
            return AboutDialog()
        }
    }
}
