/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.ViewGroup
import chernyshov.yuriy.driverassistant.utils.AppLogger

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 05.06.14
 * Time: 22:53
 *
 * This class draws detected value of the speed limit road sign.
 */
class SpeedLimitViewEU(context: Context?, attrs: AttributeSet?) : ViewGroup(context, attrs), SpeedLimitView {

    private val mWhiteOuterPaint: Paint
    private val mWhiteInnerPaint: Paint
    private val mRedPaint: Paint
    private val mTextPaint: Paint
    private var mSignHalfWidth = 0f
    private var mSignWidth = 0
    private var mSignHeight = 0
    private var mTextHeight = 0
    private var mTextWidth = 0
    private var mSpeedValue = DEFAULT_SIGN

    /**
     * This method is set recognized value of the speed.
     *
     * @param value Speed value.
     */
    override fun setSpeedValue(value: String) {
        visibility = VISIBLE
        // Adjust text size to the value
        val textSize: Float = if (value.toInt() < 100) {
            TEXT_SIZE_LARGE
        } else {
            TEXT_SIZE_MEDIUM
        }
        mTextPaint.textSize = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP, textSize, resources.displayMetrics
        )
        val result = Rect()
        mTextPaint.getTextBounds(mSpeedValue, 0, mSpeedValue.length, result)
        mTextWidth = result.width()
        mTextHeight = result.height()
        mSpeedValue = value
        invalidate()
    }

    /**
     * Reset current view to the default one.
     */
    override fun resetView() {
        mSpeedValue = DEFAULT_SIGN
        invalidate()
        visibility = GONE
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int,
                          r: Int, b: Int) {
        mSignWidth = r - l
        mSignHeight = b - t
        mSignHalfWidth = (mSignWidth shr 1).toFloat()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (TextUtils.isEmpty(mSpeedValue)) {
            AppLogger.w("$LOG_TAG can not draw, Speed value is invalid")
            return
        }
        canvas.drawCircle(mSignHalfWidth, mSignHalfWidth, mSignHalfWidth, mWhiteOuterPaint)
        var diameter = mSignHalfWidth * 0.95f
        canvas.drawCircle(mSignHalfWidth, mSignHalfWidth, diameter, mRedPaint)
        diameter = mSignHalfWidth * 0.75f
        canvas.drawCircle(mSignHalfWidth, mSignHalfWidth, diameter, mWhiteInnerPaint)
        canvas.drawText(
                mSpeedValue, (
                (mTextWidth shr 1) + (mSignWidth - mTextWidth shr 1)).toFloat(), (
                mTextHeight + (mSignHeight - mTextHeight shr 1)).toFloat(),
                mTextPaint
        )
    }

    override fun toString(): String {
        return "SpeedLimitViewEU{speedValue='$mSpeedValue'}"
    }

    companion object {
        private val LOG_TAG = SpeedLimitViewEU::class.java.simpleName

        /**
         * Default symbol for the Speed Limit sign
         */
        private const val DEFAULT_SIGN = "?"

        /**
         * Text size for the Speed values below 100 km/h
         */
        private const val TEXT_SIZE_LARGE = 40f

        /**
         * Text size for the Speed values above or equals to 100 km/h
         */
        private const val TEXT_SIZE_MEDIUM = 30f
    }

    init {
        setWillNotDraw(false)
        mWhiteOuterPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mWhiteInnerPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mRedPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mTextPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mWhiteOuterPaint.color = Color.WHITE
        mWhiteInnerPaint.color = Color.WHITE
        mRedPaint.color = Color.RED
        mTextPaint.color = Color.BLACK
        mTextPaint.textSize = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_SP,
                TEXT_SIZE_LARGE,
                resources.displayMetrics
        )
        mTextPaint.typeface = Typeface.DEFAULT_BOLD
        mTextPaint.textAlign = Paint.Align.CENTER
    }
}
