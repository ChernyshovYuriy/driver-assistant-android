/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.Typeface
import android.text.TextUtils
import android.util.AttributeSet
import android.util.TypedValue
import android.view.ViewGroup
import chernyshov.yuriy.driverassistant.utils.AppLogger

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 05.06.14
 * Time: 22:53
 *
 *
 * This class draws detected value of the speed limit road sign.
 */
class SpeedLimitViewCanada(context: Context?, attrs: AttributeSet?) : ViewGroup(context, attrs), SpeedLimitView {
    private val mWhiteBackgroundPaint: Paint
    private val mBlackBorderPaint: Paint
    private val mTextPaint: Paint
    private val mLabelPaint: Paint
    private val mWhiteBackgroundRect: Rect
    private var mSignWidth = 0
    private var mSignHeight = 0
    private var mTextHeight = 0
    private var mTextWidth = 0
    private val mLabelHeight: Int
    private val mLabelWidth: Int
    private var mBorderPadding = 0
    private var mXPadding = 0
    private var mSpeedValue = DEFAULT_SIGN

    /**
     * This method is set recognized value of the speed.
     *
     * @param value Speed value.
     */
    override fun setSpeedValue(value: String) {
        visibility = VISIBLE
        mSpeedValue = value
        // Adjust text size to the value
        val textSize: Float = if (mSpeedValue.toInt() < 100) {
            TEXT_SIZE_LARGE
        } else {
            TEXT_SIZE_MEDIUM
        }
        mTextPaint.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP, textSize, resources.displayMetrics
        )
        val result = Rect()
        mTextPaint.getTextBounds(mSpeedValue, 0, mSpeedValue.length, result)
        mTextWidth = result.width()
        mTextHeight = result.height()
        invalidate()
    }

    /**
     * Reset current view to the default one.
     */
    override fun resetView() {
        mSpeedValue = DEFAULT_SIGN
        invalidate()
        visibility = GONE
    }

    override fun onLayout(
        changed: Boolean, l: Int, t: Int,
        r: Int, b: Int
    ) {
        mSignWidth = r - l
        mSignHeight = mSignWidth * SIDE_HEIGHT_ORIGIN / SIDE_WIDTH_ORIGIN
        if (mSignHeight > b - t) {
            mSignHeight = b - t
            mSignWidth = mSignHeight * SIDE_WIDTH_ORIGIN / SIDE_HEIGHT_ORIGIN
            mXPadding = r - l - mSignWidth shr 1
        }
        mBorderPadding = (0.05f * mSignWidth).toInt()
        mWhiteBackgroundRect[mXPadding, 0, mSignWidth + mXPadding] = mSignHeight
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (TextUtils.isEmpty(mSpeedValue)) {
            AppLogger.w("$LOG_TAG can not draw, Speed value is invalid")
            return
        }
        canvas.drawRect(mWhiteBackgroundRect, mWhiteBackgroundPaint)
        canvas.drawLine(
            (
                    mBorderPadding + mXPadding).toFloat(),
            mBorderPadding.toFloat(), (
                    mSignWidth - mBorderPadding + mXPadding).toFloat(),
            mBorderPadding.toFloat(),
            mBlackBorderPaint
        )
        canvas.drawLine(
            (
                    mSignWidth - mBorderPadding + mXPadding).toFloat(),
            mBorderPadding.toFloat(), (
                    mSignWidth - mBorderPadding + mXPadding).toFloat(), (
                    mSignHeight - mBorderPadding).toFloat(),
            mBlackBorderPaint
        )
        canvas.drawLine(
            (
                    mSignWidth - mBorderPadding + mXPadding).toFloat(), (
                    mSignHeight - mBorderPadding).toFloat(), (
                    mBorderPadding + mXPadding).toFloat(), (
                    mSignHeight - mBorderPadding).toFloat(),
            mBlackBorderPaint
        )
        canvas.drawLine(
            (
                    mBorderPadding + mXPadding).toFloat(), (
                    mSignHeight - mBorderPadding).toFloat(), (
                    mBorderPadding + mXPadding).toFloat(),
            mBorderPadding.toFloat(),
            mBlackBorderPaint
        )
        canvas.drawText(
            MAXIMUM, (
                    (mLabelWidth shr 1) + (mSignWidth - mLabelWidth shr 1) + mXPadding).toFloat(), (
                    mLabelHeight + mBorderPadding * 4).toFloat(),
            mLabelPaint
        )
        canvas.drawText(
            mSpeedValue, (
                    (mTextWidth shr 1) + (mSignWidth - mTextWidth shr 1) + mXPadding).toFloat(), (
                    (mSignHeight shr 1) + mTextHeight).toFloat(),
            mTextPaint
        )
    }

    override fun toString(): String {
        return "SpeedLimitViewNA{speedValue='$mSpeedValue'}"
    }

    companion object {
        private val LOG_TAG = SpeedLimitViewCanada::class.java.simpleName

        /**
         * Default symbol for the Speed Limit sign
         */
        private const val DEFAULT_SIGN = "?"
        private const val MAXIMUM = "MAXIMUM"

        /**
         * Text size for the Speed values below 100 km/h
         */
        private const val TEXT_SIZE_LARGE = 35f

        /**
         * Text size for the Speed values above or equals to 100 km/h
         */
        private const val TEXT_SIZE_MEDIUM = 25f
        private const val TEXT_SIZE_SMALL = 10f
        private const val SIDE_WIDTH_ORIGIN = 60
        private const val SIDE_HEIGHT_ORIGIN = 75
    }

    init {
        setWillNotDraw(false)
        mWhiteBackgroundRect = Rect()
        mTextPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mLabelPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mBlackBorderPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mWhiteBackgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mTextPaint.color = Color.BLACK
        mLabelPaint.color = Color.BLACK
        mBlackBorderPaint.color = Color.BLACK
        mBlackBorderPaint.strokeWidth = 3f
        mWhiteBackgroundPaint.color = Color.WHITE
        mTextPaint.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            TEXT_SIZE_LARGE,
            resources.displayMetrics
        )
        mTextPaint.typeface = Typeface.DEFAULT_BOLD
        mTextPaint.textAlign = Paint.Align.CENTER
        mLabelPaint.textSize = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_SP,
            TEXT_SIZE_SMALL,
            resources.displayMetrics
        )
        mLabelPaint.typeface = Typeface.MONOSPACE
        mLabelPaint.textAlign = Paint.Align.CENTER
        val result = Rect()
        mLabelPaint.getTextBounds(MAXIMUM, 0, MAXIMUM.length, result)
        mLabelWidth = result.width()
        mLabelHeight = result.height()
    }
}
