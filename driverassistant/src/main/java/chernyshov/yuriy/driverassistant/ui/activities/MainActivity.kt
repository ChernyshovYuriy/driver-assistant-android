/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.activities

import android.graphics.ImageFormat
import android.graphics.YuvImage
import android.os.Bundle
import android.os.PowerManager
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.commit
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.core.detection.DetectionCallback
import chernyshov.yuriy.driverassistant.core.detection.DetectionResult
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.sensors.LightSensorManager
import chernyshov.yuriy.driverassistant.services.AppService
import chernyshov.yuriy.driverassistant.services.AppService.SpeedListener
import chernyshov.yuriy.driverassistant.services.AppService.SpeedListener.ExceedLevel
import chernyshov.yuriy.driverassistant.ui.dialogs.InitWarningDialog
import chernyshov.yuriy.driverassistant.ui.fragments.PermissionsFragment
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.CameraUtils
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.collections.set
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class MainActivity : BaseAppActivity() {

    private var mCameraX: Camera? = null
    private lateinit var mViewFinder: PreviewView
    private var mCameraProvider: ProcessCameraProvider? = null
    private var mLensFacing = CameraSelector.LENS_FACING_BACK
    private var mPreview: Preview? = null
    private var mImageCapture: ImageCapture? = null
    private var mImageAnalyzer: ImageAnalysis? = null
    private var mCameraZoomMin = CameraUtils.ZOOM_MIN
    private var mCameraZoomMax = CameraUtils.ZOOM_MAX

    /** Blocking camera operations are performed using this executor */
    private lateinit var mCameraExecutor: ExecutorService

    private var mWakeLock: PowerManager.WakeLock? = null
    private var mSpeedView: TextView? = null
    private var mSpeedViewDefaultColor = 0

    // TODO: Move to AppService
    private val mLightSensorManager: LightSensorManager = LightSensorManager()
    private var mNightViewCurtain: View? = null
    private var mLightValueView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        mViewFinder = findViewById(R.id.view_finder)
        mSpeedView = findViewById(R.id.current_speed_view)
        mSpeedViewDefaultColor = mSpeedView?.textColors!!.defaultColor
        mLightValueView = findViewById(R.id.light_value_view)
        // Initialize our background executor
        mCameraExecutor = Executors.newSingleThreadExecutor()

        createUI()

        // Wait for the views to be properly laid out
        mViewFinder.post {

            // Set up the camera and its use cases
            setUpCamera()
        }
    }

    override fun onPause() {
        super.onPause()
        mAppService?.onPause(this)
        mLightSensorManager.unregister()
    }

    override fun onResume() {
        super.onResume()
        // Make sure that all permissions are still present, since the
        // user could have removed them while the app was in paused state.
        if (!PermissionsFragment.hasPermissions(applicationContext)) {
            val fragment: PermissionsFragment =
                if (supportFragmentManager.findFragmentByTag(PermissionsFragment.TAG) == null) {
                    PermissionsFragment.newInstance()
                } else {
                    supportFragmentManager.findFragmentByTag(PermissionsFragment.TAG) as PermissionsFragment
                }
            supportFragmentManager.commit {
                setReorderingAllowed(true)
                replace(R.id.fragment_container_view, fragment, PermissionsFragment.TAG)
            }
        }

        mAppService?.onResume(this)
        mLightSensorManager.register()
    }

    override fun onDestroy() {
        super.onDestroy()
        // Shut down our background executor
        mCameraExecutor.shutdown()
        if (mWakeLock != null && mWakeLock!!.isHeld) {
            try {
                mWakeLock!!.release()
            } catch (e: Exception) {
                /* Ignore */
            }
        }
    }

    /**
     * On click handler for the road sign tap event.
     *
     * @param view [android.view.View] associated with a Road sign
     */
    fun onSpeedLimitEUClickHandler(view: View?) {
        AppLogger.d("Clear Speed Limit EU view")
        mLastDetectedSpeedLimitValue[RoadSign.SPEED_LIMIT_EU] = ""
        mSpdLmtVwEU?.resetView()
    }

    /**
     * On click handler for the road sign tap event.
     *
     * @param view [android.view.View] associated with a Road sign
     */
    fun onSpeedLimitNAClickHandler(view: View?) {
        AppLogger.d("Clear Speed Limit NA view")
        mLastDetectedSpeedLimitValue[RoadSign.SPEED_LIMIT_CANADA] = ""
        mSpdLmtVwCanada?.resetView()
    }

    fun setInitWarningDialogShown() {
        mAppService?.isInitWarningDialogShown = true
    }

    override fun onAppServiceBounded(service: AppService?) {
        super.onAppServiceBounded(service)
        if (mAppService == null) {
            return
        }
        mAppService?.onResume(this)
        mAppService?.setDetectionCallback(
            object : DetectionCallback {
                override fun onComplete(roadSign: RoadSign, detectionResult: DetectionResult) {
                    when (roadSign) {
                        RoadSign.SPEED_LIMIT_CANADA,
                        RoadSign.SPEED_LIMIT_EU,
                        RoadSign.STOP -> mAppService?.recognizeText(roadSign, detectionResult)
                        RoadSign.NO_LEFT_TURN,
                        RoadSign.NO_RIGHT_TURN,
                        RoadSign.NO_U_TURN,
                        RoadSign.NO_PARKING,
                        RoadSign.NO_STOPPING -> showSignView(roadSign)
                        else -> AppLogger.w("$CLASS_NAME unknown sign recognized:$roadSign")
                    }
                }
            }
        )
        mAppService?.setSpeedListener(
            object : SpeedListener {
                override fun onGpsSpeed(speed: String) {
                    runOnUiThread {
                        if (mSpeedView != null) {
                            mSpeedView!!.text = speed
                        }
                    }
                }

                override fun onSpeedExceed(level: ExceedLevel) {
                    if (mSpeedView == null) {
                        return
                    }
                    runOnUiThread {
                        when (level) {
                            ExceedLevel.MID -> mSpeedView!!.setTextColor(-0x8000)
                            ExceedLevel.HIGH -> mSpeedView!!.setTextColor(-0x10000)
                            else -> mSpeedView!!.setTextColor(
                                mSpeedViewDefaultColor
                            )
                        }
                    }
                }
            }
        )
        setCameraZoom(mAppService!!.cameraZoom)
    }

    /**
     * Apply currently stored settings to the Activity
     */
    override fun updateStateFromSettings() {
        super.updateStateFromSettings()
        AppLogger.d("$CLASS_NAME UpdateStateFromSettings")
        if (mIsOnSaveInstanceState.get()) {
            AppLogger.w("$CLASS_NAME Can not update settings after onSaveInstanceState")
            return
        }
        if (mAppService == null) {
            AppLogger.w("$CLASS_NAME Can not update settings, Service is null")
            return
        }
        if (!mAppService!!.isInitWarningDialogShown) {
            showInitWarningDialog()
        }
        setCameraZoom(mAppService!!.cameraZoom)
    }

    /**
     * Update Camera Zoom
     *
     * @param value Zoom value
     */
    private fun setCameraZoom(value: Int) {
        if (mCameraX == null) {
            return
        }
        val cameraControl = mCameraX?.cameraControl
        val valueConverted = CameraUtils.calculateFactor(value, mCameraZoomMin, mCameraZoomMax)
        AppLogger.d("Set Zoom of $value % to $valueConverted")
        cameraControl?.setZoomRatio(valueConverted)
    }

    private fun showInitWarningDialog() {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        val transaction = supportFragmentManager.beginTransaction()
        val fragment = supportFragmentManager.findFragmentByTag(INIT_WARNING_DIALOG_TAG)
        if (fragment != null) {
            transaction.remove(fragment)
        }
        transaction.addToBackStack(null)

        // Create and show the dialog.
        val initWarningDialog: DialogFragment = InitWarningDialog.newInstance()
        initWarningDialog.isCancelable = false
        initWarningDialog.show(transaction, INIT_WARNING_DIALOG_TAG)
        setInitWarningDialogShown()
    }

    private fun createUI() {
        // This code together with the one in onDestroy().
        // will make the CPU be always on until this Activity gets destroyed.
        val powerManager = getSystemService(POWER_SERVICE) as PowerManager
        mWakeLock = powerManager.newWakeLock(
            PowerManager.PARTIAL_WAKE_LOCK, "DRIVER_ASSISTANT:POWER_MANAGER"
        )
        mWakeLock?.acquire(1000)
        mSpdLmtVwEU = findViewById(R.id.speed_limit_view_eu)
        mSpdLmtVwCanada = findViewById(R.id.speed_limit_view_na)
        mSignsViews[RoadSign.STOP.id] = findViewById(R.id.stop_sign_view)
        mSignsViews[RoadSign.NO_LEFT_TURN.id] = findViewById(R.id.no_left_turn_sign_view)
        mSignsViews[RoadSign.NO_RIGHT_TURN.id] = findViewById(R.id.no_right_turn_sign_view)
        mSignsViews[RoadSign.NO_U_TURN.id] = findViewById(R.id.no_u_turn_sign_view)
        mSignsViews[RoadSign.NO_PARKING.id] = findViewById(R.id.no_parking_sign_view)
        mSignsViews[RoadSign.NO_STOPPING.id] = findViewById(R.id.no_stopping_sign_view)
        mNightViewCurtain = findViewById(R.id.night_view_curtain)
        mLightSensorManager.init(applicationContext)
    }

    private inner class LightSensorEventListenerImpl : LightSensorManager.Listener {

        private var mLight = 0.0f

        override fun onChanged(lightQuantity: Float) {
            mLight = lightQuantity
            if (mAppService == null) {
                return
            }
            // TODO: This is temporary solution. Better one is to disable LightService.
            if (!mAppService!!.isDynamicScreenBrightness) {
                return
            }
            if (mLightValueView != null) {
                mLightValueView!!.text = mLight.toString()
            }
            mNightViewCurtain!!.visibility = if (mLight < LIGHT_THRESHOLD) View.VISIBLE else View.GONE
        }

        private val LIGHT_THRESHOLD = 5
    }

    companion object {
        private val CLASS_NAME = MainActivity::class.java.simpleName
        private const val INIT_WARNING_DIALOG_TAG = "INIT_WARNING_DIALOG_TAG"

        private const val RATIO_4_3_VALUE = 4.0 / 3.0
        private const val RATIO_16_9_VALUE = 16.0 / 9.0
    }

    /**
     * Default constructor.
     */
    init {
        mLightSensorManager.setExternalListener(LightSensorEventListenerImpl())
    }

    /**
     *  [androidx.camera.core.ImageAnalysis] requires enum value of
     *  [androidx.camera.core.AspectRatio]. Currently it has values of 4:3 & 16:9.
     *
     *  Detecting the most suitable ratio for dimensions provided in @params by counting absolute
     *  of preview ratio to one of the provided values.
     *
     *  @param width - preview width
     *  @param height - preview height
     *  @return suitable aspect ratio
     */
    private fun aspectRatio(width: Int, height: Int): Int {
        val previewRatio = max(width, height).toDouble() / min(width, height)
        if (abs(previewRatio - RATIO_4_3_VALUE) <= abs(previewRatio - RATIO_16_9_VALUE)) {
            return AspectRatio.RATIO_4_3
        }
        return AspectRatio.RATIO_16_9
    }

    /** Initialize CameraX, and prepare to bind the camera use cases  */
    private fun setUpCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(applicationContext)
        cameraProviderFuture.addListener({

            // CameraProvider
            mCameraProvider = cameraProviderFuture.get()

            // Build and bind the camera use cases
            bindCameraUseCases()
        }, ContextCompat.getMainExecutor(applicationContext))
    }

    /** Declare and bind preview, capture and analysis use cases */
    private fun bindCameraUseCases() {

        // Get screen metrics used to setup camera for full screen resolution
        val metrics = DisplayMetrics().also { mViewFinder.display.getRealMetrics(it) }
        AppLogger.d("Screen metrics: ${metrics.widthPixels} x ${metrics.heightPixels}")

        val screenAspectRatio = aspectRatio(metrics.widthPixels, metrics.heightPixels)
        AppLogger.d("Preview aspect ratio: $screenAspectRatio")

        val rotation = mViewFinder.display.rotation

        // CameraProvider
        val cameraProvider = mCameraProvider
            ?: throw IllegalStateException("Camera initialization failed.")

        // CameraSelector
        val cameraSelector = CameraSelector.Builder().requireLensFacing(mLensFacing).build()

        // Preview
        mPreview = Preview.Builder()
            // We request aspect ratio but no resolution
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation
            .setTargetRotation(rotation)
            .build()

        // ImageCapture
        mImageCapture = ImageCapture.Builder()
            .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
            // We request aspect ratio but no resolution to match preview config, but letting
            // CameraX optimize for whatever specific resolution best fits our use cases
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()

        // ImageAnalysis
        mImageAnalyzer = ImageAnalysis.Builder()
            // We request aspect ratio but no resolution
            .setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            .setTargetRotation(rotation)
            .build()
            // The analyzer can then be assigned to the instance
            .also {
                it.setAnalyzer(mCameraExecutor, LuminosityAnalyzer(mAppService!!))
            }

        // Must unbind the use-cases before rebinding them
        cameraProvider.unbindAll()

        try {
            // A variable number of use-cases can be passed here -
            // camera provides access to CameraControl & CameraInfo
            mCameraX = cameraProvider.bindToLifecycle(
                this, cameraSelector, mPreview, mImageCapture, mImageAnalyzer
            )

            // Attach the viewfinder's surface provider to preview use case
            mPreview?.setSurfaceProvider(mViewFinder.surfaceProvider)
        } catch (exc: Exception) {
            AppLogger.e("Use case binding failed: $exc")
        }

        CameraUtils.dumpCameraInfo(mCameraX)

        val pair = CameraUtils.getZoomMinMax(mCameraX)
        mCameraZoomMin = pair.first
        mCameraZoomMax = pair.second

        setCameraZoom(mAppService!!.cameraZoom)
    }

    /**
     * Our custom image analysis class.
     *
     * <p>All we need to do is override the function `analyze` with our desired operations. Here,
     * we compute the average luminosity of the image by looking at the Y plane of the YUV frame.
     */
    private class LuminosityAnalyzer(private val mAppService: AppService) : ImageAnalysis.Analyzer {

        private lateinit var mNv21: ByteArray

        /**
         * Analyzes an image to produce a result.
         *
         * <p>The caller is responsible for ensuring this analysis method can be executed quickly
         * enough to prevent stalls in the image acquisition pipeline. Otherwise, newly available
         * images will not be acquired and analyzed.
         *
         * <p>The image passed to this method becomes invalid after this method returns. The caller
         * should not store external references to this image, as these references will become
         * invalid.
         *
         * @param image image being analyzed VERY IMPORTANT: Analyzer method implementation must
         * call image.close() on received images when finished using them. Otherwise, new images
         * may not be received or the camera may stall, depending on back pressure setting.
         *
         */
        override fun analyze(image: ImageProxy) {
            val planes = image.planes
            val yBuffer = planes[0].buffer
            val uBuffer = planes[1].buffer
            val vBuffer = planes[2].buffer

            val ySize = yBuffer.remaining()
            val uSize = uBuffer.remaining()
            val vSize = vBuffer.remaining()

            if (!this@LuminosityAnalyzer::mNv21.isInitialized) {
                mNv21 = ByteArray(ySize + uSize + vSize)
            }

            // U and V are swapped
            yBuffer[mNv21, 0, ySize]
            vBuffer[mNv21, ySize, vSize]
            uBuffer[mNv21, ySize + vSize, uSize]

            mAppService.onVideoData(YuvImage(mNv21, ImageFormat.NV21, image.width, image.height, null).yuvData)

            image.close()
        }
    }
}
