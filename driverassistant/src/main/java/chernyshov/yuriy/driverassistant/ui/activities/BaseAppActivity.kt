/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.activities

import android.content.ComponentName
import android.content.ServiceConnection
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.IBinder
import android.os.PersistableBundle
import android.speech.tts.TextToSpeech
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.core.OCRDetectorProcessorListener
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.services.AppService
import chernyshov.yuriy.driverassistant.services.AppService.AppServiceBinder
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.ui.SafeToast
import chernyshov.yuriy.driverassistant.ui.views.SpeedLimitView
import chernyshov.yuriy.driverassistant.ui.dialogs.AboutDialog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.atomic.*

/**
 * Created with Android Studio.
 * Author: Chernyshov Yuriy - Mobile Development
 * Date: 12/18/13
 * Time: 3:39 PM
 */
abstract class BaseAppActivity : AppCompatActivity() {

    protected var mAppService: AppService? = null
    private var mCameraWidth = 0
    private var mCameraHeight = 0

    /**
     * Last detected result of the Speed limit sign.
     */
    protected val mLastDetectedSpeedLimitValue = mutableMapOf<RoadSign, String>()
    private var mBound = false
    private val mServiceConnection = ServiceConnectionImpl()

    /**
     * View which displays recognized value of the Speed (European Union).
     */
    protected var mSpdLmtVwEU: SpeedLimitView? = null

    /**
     * View which displays recognized value of the Speed (Canada).
     */
    protected var mSpdLmtVwCanada: SpeedLimitView? = null

    /**
     * Array of the available Signs Views.
     */
    protected var mSignsViews = arrayOfNulls<ImageView>(RoadSign.values().size)
    private var mTextToSpeech: TextToSpeech? = null
    private var mLastSpeakSign: String? = null
    private var mLastSpeakSignTime: Long = 0
    protected var mIsOnSaveInstanceState = AtomicBoolean(false)

    /**
     *
     */
    private val mIsSignShown = arrayOfNulls<AtomicBoolean>(RoadSign.values().size)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppLogger.i("$CLASS_NAME onCreate, bundle: $savedInstanceState")
        mIsOnSaveInstanceState.set(false)
        for (i in RoadSign.values().indices) {
            if (mIsSignShown[i] == null) {
                mIsSignShown[i] = AtomicBoolean(false)
            } else {
                mIsSignShown[i]!!.set(false)
            }
        }

        // Bind to AppService
        bindService(
            AppService.createIntent(applicationContext),
            mServiceConnection, BIND_AUTO_CREATE
        )
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        mIsOnSaveInstanceState.set(true)
    }

    override fun onPause() {
        super.onPause()
        AppLogger.i("$CLASS_NAME onPause")
    }

    override fun onResume() {
        super.onResume()
        AppLogger.i("$CLASS_NAME onResume")
        mIsOnSaveInstanceState.set(false)
        if (mAppService == null) {
            return
        }
        updateStateFromSettings()
    }

    override fun onDestroy() {
        super.onDestroy()
        AppLogger.i("$CLASS_NAME onDestroy")
        deInitTTSEngine()

        // Unbind from the Services
        if (mBound) {
            AppLogger.i("$CLASS_NAME unbound services")
            unbindService(mServiceConnection)
            mBound = false
            setAppService(null)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                startActivity(SettingsActivity.createIntent(applicationContext))
                return true
            }
            R.id.action_about -> {
                // DialogFragment.show() will take care of adding the fragment
                // in a transaction.  We also want to remove any currently showing
                // dialog, so make our own transaction and take care of that here.
                val fragmentTransaction = supportFragmentManager
                    .beginTransaction()
                val fragmentByTag = supportFragmentManager
                    .findFragmentByTag(ABOUT_DIALOG_TAG)
                if (fragmentByTag != null) {
                    fragmentTransaction.remove(fragmentByTag)
                }
                fragmentTransaction.addToBackStack(null)

                // Create and show the dialog.
                val aboutDialog: DialogFragment = AboutDialog.newInstance()
                aboutDialog.show(fragmentTransaction, ABOUT_DIALOG_TAG)
                return true
            }
            R.id.action_process_video -> {
                startActivity(
                    VideoViewActivity.createIntent(
                        applicationContext, mCameraWidth, mCameraHeight
                    )
                )
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSIONS_REQUEST_CAMERA -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {

                    // Permission was granted.
                    // Do the contacts-related task you need to do.
                    SafeToast.showToastAnyThread(
                        applicationContext, "Camera permission granted"
                    )
                } else {

                    // Permission denied.
                    // Disable the functionality that depends on this permission.
                    SafeToast.showToastAnyThread(
                        applicationContext, "Camera permission disabled"
                    )
                }
            }
        }
    }

    private fun setAppService(value: AppService?) {
        mAppService = value
    }

    protected open fun onAppServiceBounded(service: AppService?) {
        setAppService(service)
        completeInit()
    }

    /**
     * Complete initialization process.
     */
    private fun completeInit() {
        if (mAppService == null) {
            AppLogger.e("$CLASS_NAME OnServiceConnected - App Service is null")
            return
        }
        mAppService?.setOcrDetectorProcessorListener(
            object : OCRDetectorProcessorListener {

                override fun onDetect(roadSign: RoadSign, detectedText: String) {
                    when (roadSign) {
                        RoadSign.SPEED_LIMIT_CANADA,
                        RoadSign.SPEED_LIMIT_EU -> {
                            updateSpeedLimitView(
                                if (roadSign == RoadSign.SPEED_LIMIT_EU) mSpdLmtVwEU!! else mSpdLmtVwCanada!!,
                                detectedText
                            )
                        }
                        RoadSign.STOP -> showSignView(RoadSign.STOP)
                        else -> AppLogger.w("$CLASS_NAME Unknown road sign:$roadSign")
                    }
                }
            }
        )
        updateStateFromSettings()
    }

    protected open fun updateStateFromSettings() {
        if (mAppService == null) {
            AppLogger.w("$CLASS_NAME Can not update settings, Service is null")
            return
        }
        if (mAppService!!.isTTSEngineEnabled) {
            initTTSEngine()
        } else {
            deInitTTSEngine()
        }
    }

    /**
     * Method to initialize TTS Engine.
     */
    private fun initTTSEngine() {
        AppLogger.i("$CLASS_NAME Initialize TTS")
        if (mTextToSpeech != null) {
            return
        }
        mTextToSpeech = TextToSpeech(applicationContext) { }
        mTextToSpeech!!.language = Locale.CANADA
    }

    /**
     * Method to de-initialize TTS Engine.
     */
    private fun deInitTTSEngine() {
        if (mTextToSpeech != null) {
            AppLogger.i("$CLASS_NAME De-Initialize TTS")
            mTextToSpeech!!.stop()
            mTextToSpeech!!.shutdown()
        }
        mTextToSpeech = null
    }

    private fun speakSign(text: String) {
        if (mTextToSpeech == null) {
            return
        }
        if (TextUtils.isEmpty(text)) {
            return
        }
        val currentTime = System.currentTimeMillis()
        if (TextUtils.equals(
                text,
                mLastSpeakSign
            ) && mLastSpeakSignTime != 0L && currentTime - mLastSpeakSignTime < 5000
        ) {
            return
        }
        mLastSpeakSign = text
        mLastSpeakSignTime = currentTime
        // TODO: Replace 123 with value
        mTextToSpeech!!.speak(mLastSpeakSign, TextToSpeech.QUEUE_ADD, Bundle(), "123")
    }

    /***
     * Update Speed Limit sign view with detected value.
     *
     * @param detectedValue Detected value.
     */
    protected fun updateSpeedLimitView(speedLimitView: SpeedLimitView, detectedValue: String) {
        GlobalScope.launch(Dispatchers.Main) {
            val message = "Speed limit $detectedValue kilometers"
            speakSign(message)
            speedLimitView.setSpeedValue(detectedValue)
        }
    }

    protected fun showSignView(roadSign: RoadSign) {
        val index = roadSign.id
        if (mIsSignShown[index]!!.get()) {
            return
        }
        showSign(index)
        speakSignDetected(roadSign)
        GlobalScope.launch(Dispatchers.Main) {
            delay(SIGN_VIEW_TIMEOUT)
            hideSign(index)
        }
    }

    private fun showSign(index: Int) {
        if (mSignsViews[index] == null) {
            return
        }
        mIsSignShown[index]!!.set(true)
        GlobalScope.launch(Dispatchers.Main) {
            mSignsViews[index]!!.visibility = View.VISIBLE
        }
    }

    private fun hideSign(index: Int) {
        if (mSignsViews[index] == null) {
            return
        }
        mIsSignShown[index]!!.set(false)
        GlobalScope.launch(Dispatchers.Main) {
            mSignsViews[index]!!.visibility = View.GONE
        }
    }

    private fun speakSignDetected(roadSign: RoadSign) {
        var message = ""
        when (roadSign) {
            RoadSign.NO_RIGHT_TURN -> message = "No right turn"
            RoadSign.NO_U_TURN -> message = "No U turn "
            RoadSign.STOP -> message = "Stop sign"
            RoadSign.NO_LEFT_TURN -> message = "No left turn"
            RoadSign.SPEED_LIMIT_EU -> {
            }
            RoadSign.SPEED_LIMIT_CANADA -> {
            }
            RoadSign.NO_PARKING -> message = "No parking"
            RoadSign.NO_STOPPING -> message = "No stopping"
            RoadSign.NO_LEFT_RIGHT_TURN -> message = "No left and right turn"
            RoadSign.NO_STRAIGHT -> message = "No straight"
            RoadSign.UNKNOWN -> {
            }
        }
        speakSign(message)
    }

    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private inner class ServiceConnectionImpl : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, iBinder: IBinder) {
            AppLogger.i("$CLASS_NAME Service connected:$iBinder")
            mBound = true
            if (iBinder is AppServiceBinder) {
                // We've bound to AppService, cast the IBinder and get AppService instance
                val appService = iBinder.service
                onAppServiceBounded(appService)
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            AppLogger.i("$CLASS_NAME Service disconnected:$componentName")
            mBound = false
        }
    }

    companion object {
        private val CLASS_NAME = BaseAppActivity::class.java.simpleName
        private const val ABOUT_DIALOG_TAG = "ABOUT_DIALOG_TAG"
        private const val PERMISSIONS_REQUEST_CAMERA = 100

        /**
         * Timeout of the Stop sign display, in milliseconds
         */
        private const val SIGN_VIEW_TIMEOUT = 5000L
    }
}
