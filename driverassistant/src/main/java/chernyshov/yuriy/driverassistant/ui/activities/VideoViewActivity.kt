/*
 * Copyright 2017 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package chernyshov.yuriy.driverassistant.ui.activities

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.TextUtils
import android.view.Menu
import android.view.TextureView
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import chernyshov.yuriy.driverassistant.MainApp
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.core.detection.DetectionCallback
import chernyshov.yuriy.driverassistant.core.detection.DetectionResult
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.services.AppService
import chernyshov.yuriy.driverassistant.utils.AnalyticsUtils
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.AppUtils
import chernyshov.yuriy.driverassistant.utils.IntentBuilder
import chernyshov.yuriy.driverassistant.ui.SafeToast
import chernyshov.yuriy.driverassistant.ui.exoplayer.TrackSelectionHelper
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.DefaultRenderersFactory.ExtensionRendererMode
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.PlaybackPreparer
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.Player.DefaultEventListener
import com.google.android.exoplayer2.Player.DiscontinuityReason
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer.DecoderInitializationException
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil.DecoderQueryException
import com.google.android.exoplayer2.source.BehindLiveWindowException
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.MediaSourceEventListener
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.MappingTrackSelector.MappedTrackInfo
import com.google.android.exoplayer2.trackselection.TrackSelection
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.ui.PlayerControlView
import com.google.android.exoplayer2.ui.PlayerView
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.util.EventLogger
import com.google.android.exoplayer2.util.Util
import org.opencv.android.Utils
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.imgproc.Imgproc
import java.lang.ref.WeakReference
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy
import java.net.URISyntaxException

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 1/11/15
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 *
 * [VideoViewActivity] is a class that represents
 * UI components and provides interaction with Video file processing.
 */
class VideoViewActivity : BaseAppActivity(), View.OnClickListener, PlaybackPreparer, PlayerControlView.VisibilityListener {
    /**
     * Width of the Video view.
     */
    private var mVideoWidth = 0

    /**
     * Height of the Video view.
     */
    private var mVideoHeight = 0

    /**
     * Handler to manage Video view processing.
     */
    private val videoViewHandler = Handler(Looper.getMainLooper())

    companion object {
        /**
         * String tag to use when logging.
         */
        private val CLASS_NAME = VideoViewActivity::class.java.simpleName

        /**
         * Key for the Intent;s bundle store to keep Video Width's value.
         */
        private const val BUNDLE_KEY_VIDEO_WIDTH = "BUNDLE_KEY_VIDEO_WIDTH"

        /**
         * Key for the Intent;s bundle store to keep Video Height's value.
         */
        private const val BUNDLE_KEY_VIDEO_HEIGHT = "BUNDLE_KEY_VIDEO_HEIGHT"

        /**
         * Select Video file request code.
         * Used in [android.app.Activity.onActivityResult]
         * callback method.
         */
        private const val FILE_SELECT_CODE = 100

        /**
         * Period between frames when process video data, in milliseconds.
         */
        private const val VIDEO_VIEW_PERIOD = 150

        // ExoPlayer fields
        const val PREFER_EXTENSION_DECODERS = "prefer_extension_decoders"
        private val BANDWIDTH_METER = DefaultBandwidthMeter()
        private var DEFAULT_COOKIE_MANAGER: CookieManager? = null

        /**
         * Factory method to create intent to start [VideoViewActivity].
         *
         * @param context     Callee context.
         * @param videoWidth  Width of the Video.
         * @param videoHeight Height of the Video.
         * @return Instance of the Intent.
         */
        @JvmStatic
        fun createIntent(context: Context?,
                         videoWidth: Int, videoHeight: Int): Intent {
            val intent = Intent(context, VideoViewActivity::class.java)
            intent.putExtra(BUNDLE_KEY_VIDEO_WIDTH, videoWidth)
            intent.putExtra(BUNDLE_KEY_VIDEO_HEIGHT, videoHeight)
            return intent
        }

        private fun isBehindLiveWindow(e: ExoPlaybackException): Boolean {
            if (e.type != ExoPlaybackException.TYPE_SOURCE) {
                return false
            }
            var cause: Throwable? = e.sourceException
            while (cause != null) {
                if (cause is BehindLiveWindowException) {
                    return true
                }
                cause = cause.cause
            }
            return false
        }

        init {
            DEFAULT_COOKIE_MANAGER = CookieManager()
            DEFAULT_COOKIE_MANAGER!!.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER)
        }
    }

    private var mMainHandler: Handler? = null
    private var mEventLogger: EventLogger? = null
    private var mPlayerView: PlayerView? = null
    private var mDebugRootView: LinearLayout? = null
    private var mMediaDataSourceFactory: DataSource.Factory? = null
    private var mPlayer: SimpleExoPlayer? = null
    private var mTrackSelector: DefaultTrackSelector? = null
    private var mTrackSelectionHelper: TrackSelectionHelper? = null
    private var mInErrorState = false
    private var mLastSeenTrackGroupArray: TrackGroupArray? = null
    private var mShouldAutoPlay = false
    private var mResumeWindow = 0
    private var mResumePosition: Long = 0
    private var mUrl = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Set view's content
        setContentView(R.layout.activity_video_view)

        // set up views
        mSpdLmtVwEU = findViewById(R.id.speed_limit_view_video_view_eu)
        mSpdLmtVwCanada = findViewById(R.id.speed_limit_view_video_view_na)
        mSignsViews[RoadSign.STOP.id] = findViewById(R.id.stop_sign_video_view)
        mSignsViews[RoadSign.NO_LEFT_TURN.id] = findViewById(R.id.no_left_turn_sign_video_view)
        mSignsViews[RoadSign.NO_RIGHT_TURN.id] = findViewById(R.id.no_right_turn_sign_video_view)
        mSignsViews[RoadSign.NO_U_TURN.id] = findViewById(R.id.no_u_turn_sign_video_view)
        mSignsViews[RoadSign.NO_PARKING.id] = findViewById(R.id.no_parking_sign_video_view)
        mSignsViews[RoadSign.NO_STOPPING.id] = findViewById(R.id.no_stopping_sign_video_view)

        // Set up post runnable in order to get Video view dimensions and
        // to initialize VideoFrame processor
        AppLogger.d("Create Video View activity")
        mShouldAutoPlay = true
        clearResumePosition()
        mMediaDataSourceFactory = (application as MainApp).buildDataSourceFactory(null)
        mMainHandler = Handler(Looper.getMainLooper())
        if (CookieHandler.getDefault() !== DEFAULT_COOKIE_MANAGER) {
            CookieHandler.setDefault(DEFAULT_COOKIE_MANAGER)
        }
        val rootView = findViewById<View>(R.id.root_layout)
        rootView.setOnClickListener(this)
        mDebugRootView = findViewById(R.id.controls_root)
        mPlayerView = findViewById(R.id.player_view)
        mPlayerView?.setControllerVisibilityListener(this)
        mPlayerView?.requestFocus()

//        mPlayerView.post(
//                () -> updateDimensions(mPlayerView.getWidth(), mPlayerView.getHeight())
//        );
    }

    private fun updateDimensions(width: Int, height: Int) {
        mVideoWidth = width
        mVideoHeight = height
        if (mAppService != null) {
            mAppService!!.initVideoFrameProcessor(mVideoWidth, mVideoHeight)
        }
    }

    public override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }

        // Remove callback from the Video View's handler
        videoViewHandler.removeCallbacks(videoViewRunnable)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == FILE_SELECT_CODE) {
            if (resultCode == RESULT_OK) {
                // Get the Uri of the selected file
                val uri = data!!.data
                if (uri == null) {
                    SafeToast.showToastAnyThread(
                            applicationContext, "File URI is null"
                    )
                    return
                }
                AppLogger.d("$CLASS_NAME File Uri: $uri")
                // Get the path
                var path = ""
                try {
                    path = AppUtils.getPath(applicationContext, uri).toString()
                } catch (e: URISyntaxException) {
                    AnalyticsUtils.logException(e)
                }
                AppLogger.d("$CLASS_NAME File Path: $path")
                if (!TextUtils.isEmpty(path)) {
                    playVideo(path)
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {

        // Remove an item corresponded to the current view
        menu.removeItem(R.id.action_process_video)
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onAppServiceBounded(service: AppService?) {
        super.onAppServiceBounded(service)
        if (mAppService == null) {
            return
        }
        if (mVideoWidth != 0) {
            mAppService!!.initVideoFrameProcessor(mVideoWidth, mVideoHeight)
        }
        // Set up callback for the Recognition events
        mAppService!!.setDetectionCallback(mRecognitionCallback)
    }

    /**
     * Click handler for the Select Video file Button press event.
     */
    fun onSelectVideoFileClickHandler() {
        try {
            startActivityForResult(
                    Intent.createChooser(
                            IntentBuilder.createSelectVideoFileIntent(),
                            "Select a File to Process"
                    ),
                    FILE_SELECT_CODE)
        } catch (ex: ActivityNotFoundException) {
            // Potentially direct the user to the Market with a Dialog
            SafeToast.showToastAnyThread(
                    applicationContext,
                    "Please install a File Manager."
            )
        }
    }

    /**
     * Play Video from the provided URL.
     *
     * @param url URL of the Video data.
     */
    private fun playVideo(url: String) {
        videoViewHandler.postDelayed(videoViewRunnable, VIDEO_VIEW_PERIOD.toLong())
        mUrl = url
        preparePlayback()
    }

    private val videoViewRunnable: Runnable = VideoViewRunnable(this)

    /**
     * Runnable object to manage Video View. Particularly, get a frame every interval.
     */
    private class VideoViewRunnable(activity: VideoViewActivity) : Runnable {

        private val mReference = WeakReference(activity)
        private var mMat: Mat? = null
        private lateinit var mPixelsBytes: ByteArray

        override fun run() {
            val activity = mReference.get() ?: return
            // Bound conditions check point
            if (activity.mAppService == null) {
                return
            }

            // Set up next call of this runnable
            activity.videoViewHandler.postDelayed(this, VIDEO_VIEW_PERIOD.toLong())

            // Get reference to the bitmap of the Video View's data.
            val bitmap = (activity.mPlayerView?.videoSurfaceView as TextureView).bitmap ?: return
            val height = bitmap.height
            val width = bitmap.width
            if (width != activity.mVideoWidth || height != activity.mVideoHeight) {
                activity.updateDimensions(width, height)
                if (mMat != null) {
                    mMat!!.release()
                }
                mMat = Mat(height, width, CvType.CV_8UC1)
                mPixelsBytes = ByteArray(width * height)
            }
            Utils.bitmapToMat(bitmap, mMat)
            bitmap.recycle()
            Imgproc.cvtColor(mMat, mMat, Imgproc.COLOR_RGB2GRAY)
            mMat!![0, 0, mPixelsBytes]
            activity.mAppService!!.onVideoData(mPixelsBytes)
        }

    }

    private val mRecognitionCallback: DetectionCallback = DetectionCallbackImpl(this)

    /**
     * Speed Recognition callbacks listener.
     */
    private class DetectionCallbackImpl(reference: VideoViewActivity) : DetectionCallback {

        private val mReference = WeakReference(reference)

        override fun onComplete(roadSign: RoadSign, detectionResult: DetectionResult) {
            AppLogger.d("$CLASS_NAME $roadSign detected")
            val reference = mReference.get() ?: return
            when (roadSign) {
                RoadSign.SPEED_LIMIT_EU,
                RoadSign.SPEED_LIMIT_CANADA,
                RoadSign.STOP -> reference.mAppService?.recognizeText(roadSign, detectionResult)
                RoadSign.NO_LEFT_TURN,
                RoadSign.NO_RIGHT_TURN,
                RoadSign.NO_U_TURN,
                RoadSign.NO_PARKING,
                RoadSign.NO_STOPPING -> reference.showSignView(roadSign)
                else -> AppLogger.w("$CLASS_NAME unknown sign recognized:$roadSign")
            }
        }
    }

    private fun clearResumePosition() {
        mResumeWindow = C.INDEX_UNSET
        mResumePosition = C.TIME_UNSET
    }

    private fun releasePlayer() {
        if (mPlayer != null) {
            mShouldAutoPlay = mPlayer!!.playWhenReady
            updateResumePosition()
            mPlayer!!.release()
            mPlayer = null
            mTrackSelector = null
            mTrackSelectionHelper = null
            mEventLogger = null
        }
    }

    private fun initializePlayer() {
        val needNewPlayer = mPlayer == null
        if (needNewPlayer) {
            val adaptiveTrackSelectionFactory: TrackSelection.Factory = AdaptiveTrackSelection.Factory(BANDWIDTH_METER)
            mTrackSelector = DefaultTrackSelector(adaptiveTrackSelectionFactory)
            mTrackSelectionHelper = TrackSelectionHelper(mTrackSelector, adaptiveTrackSelectionFactory)
            mLastSeenTrackGroupArray = null
            mEventLogger = EventLogger(mTrackSelector)
            val intent = intent
            val preferExtensionDecoders = intent.getBooleanExtra(PREFER_EXTENSION_DECODERS, false)
            @ExtensionRendererMode val extensionRendererMode = if ((application as MainApp).useExtensionRenderers()) if (preferExtensionDecoders) DefaultRenderersFactory.EXTENSION_RENDERER_MODE_PREFER else DefaultRenderersFactory.EXTENSION_RENDERER_MODE_ON else DefaultRenderersFactory.EXTENSION_RENDERER_MODE_OFF
            val renderersFactory = DefaultRenderersFactory(this,
                    null, extensionRendererMode)
            mPlayer = ExoPlayerFactory.newSimpleInstance(renderersFactory, mTrackSelector)
            mPlayer?.addListener(PlayerEventListener(this))
            mPlayer?.addListener(mEventLogger)
            mPlayer?.addMetadataOutput(mEventLogger)
            mPlayer?.addAudioDebugListener(mEventLogger)
            mPlayer?.addVideoDebugListener(mEventLogger)
            mPlayer?.volume = 0f
            mPlayer?.playWhenReady = mShouldAutoPlay
            mPlayerView!!.player = mPlayer
            mPlayerView!!.setPlaybackPreparer(this)
        }
        val uris = arrayOf(Uri.parse(mUrl))
        val extensions = arrayOf("")
        if (Util.maybeRequestReadExternalStoragePermission(this, *uris)) {
            // The mPlayer will be reinitialized if the permission is granted.
            return
        }
        val mediaSource = buildMediaSource(uris[0], extensions[0], mMainHandler, mEventLogger)
        val haveResumePosition = mResumeWindow != C.INDEX_UNSET
        if (haveResumePosition) {
            mPlayer!!.seekTo(mResumeWindow, mResumePosition)
        }
        mPlayer!!.prepare(mediaSource, !haveResumePosition, false)
        mInErrorState = false
    }

    private fun buildMediaSource(
            uri: Uri,
            overrideExtension: String,
            handler: Handler?,
            listener: MediaSourceEventListener?): MediaSource {
        return when (@C.ContentType val type = if (TextUtils.isEmpty(overrideExtension)) Util.inferContentType(uri) else Util.inferContentType(".$overrideExtension")) {
            C.TYPE_HLS -> HlsMediaSource.Factory(mMediaDataSourceFactory)
                    .createMediaSource(uri, handler, listener)
            C.TYPE_OTHER -> ExtractorMediaSource.Factory(mMediaDataSourceFactory)
                    .createMediaSource(uri, handler, listener)
            else -> {
                throw IllegalStateException("Unsupported type: $type")
            }
        }
    }

    override fun onClick(view: View) {
        if (view.parent === mDebugRootView) {
            val mappedTrackInfo = mTrackSelector!!.currentMappedTrackInfo
            if (mappedTrackInfo != null) {
                mTrackSelectionHelper!!.showSelectionDialog(
                        this, (view as Button).text, mappedTrackInfo, view.getTag() as Int)
            }
        }
    }

    private fun showControls() {
        mDebugRootView!!.visibility = View.VISIBLE
    }

    private fun updateResumePosition() {
        mResumeWindow = mPlayer!!.currentWindowIndex
        mResumePosition = Math.max(0, mPlayer!!.contentPosition)
    }

    override fun preparePlayback() {
        initializePlayer()
    }

    override fun onVisibilityChange(visibility: Int) {
        mDebugRootView!!.visibility = visibility
    }

    private class PlayerEventListener(activity: VideoViewActivity) : DefaultEventListener() {

        private val mReference = WeakReference(activity)

        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            val activity = mReference.get() ?: return
            if (playbackState == Player.STATE_ENDED) {
                activity.showControls()
            }
        }

        override fun onPositionDiscontinuity(@DiscontinuityReason reason: Int) {
            val activity = mReference.get() ?: return
            if (activity.mInErrorState) {
                // This will only occur if the user has performed a seek whilst in the error state. Update
                // the resume position so that if the user then retries, playback will resume from the
                // position to which they seeked.
                activity.updateResumePosition()
            }
        }

        override fun onPlayerError(e: ExoPlaybackException) {
            val activity = mReference.get() ?: return
            var errorString: String? = null
            if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                val cause = e.rendererException
                if (cause is DecoderInitializationException) {
                    // Special case for decoder initialization failures.
                    errorString = if (cause.decoderName == null) {
                        when {
                            cause.cause is DecoderQueryException -> {
                                activity.getString(R.string.error_querying_decoders)
                            }
                            cause.secureDecoderRequired -> {
                                activity.getString(R.string.error_no_secure_decoder,
                                        cause.mimeType)
                            }
                            else -> {
                                activity.getString(R.string.error_no_decoder,
                                        cause.mimeType)
                            }
                        }
                    } else {
                        activity.getString(R.string.error_instantiating_decoder,
                                cause.decoderName)
                    }
                }
            }
            if (errorString != null) {
                SafeToast.showToastAnyThread(activity.applicationContext, errorString)
            }
            activity.mInErrorState = true
            if (isBehindLiveWindow(e)) {
                activity.clearResumePosition()
                activity.initializePlayer()
            } else {
                activity.updateResumePosition()
                activity.showControls()
            }
        }

        override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {
            val activity = mReference.get() ?: return
            if (trackGroups !== activity.mLastSeenTrackGroupArray) {
                val mappedTrackInfo = activity.mTrackSelector!!.currentMappedTrackInfo
                if (mappedTrackInfo != null) {
                    if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_VIDEO)
                            == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        SafeToast.showToastAnyThread(
                                activity.applicationContext,
                                activity.getString(R.string.error_unsupported_video)
                        )
                    }
                    if (mappedTrackInfo.getTrackTypeRendererSupport(C.TRACK_TYPE_AUDIO)
                            == MappedTrackInfo.RENDERER_SUPPORT_UNSUPPORTED_TRACKS) {
                        SafeToast.showToastAnyThread(
                                activity.applicationContext,
                                activity.getString(R.string.error_unsupported_audio)
                        )
                    }
                }
                activity.mLastSeenTrackGroupArray = trackGroups
            }
        }
    }
}
