/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.Rect
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.ViewGroup
import kotlin.math.cos
import kotlin.math.sin

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 7/25/15
 * E-Mail: chernyshov.yuriy@gmail.com
 */
class StopSignView(context: Context?, attrs: AttributeSet?) : ViewGroup(context, attrs) {

    private val mRedPaint: Paint = Paint()
    private val mWhitePaint: Paint = Paint()
    private val mTextPaint: Paint = Paint()
    private val mTextBounds: Rect
    private val mWhiteFillPath: Path = Path()
    private val mRedFillPath: Path = Path()
    private var mSignHalfWidth = 0
    private val mRedPoints = Array(OCTAGON_SIZE) { FloatArray(2) }
    private val mWhitePoints = Array(OCTAGON_SIZE) { FloatArray(2) }
    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        val mSignWidth = r - l
        mSignHalfWidth = mSignWidth shr 1
        calculateHexagonPoints()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        mTextPaint.getTextBounds(STOP, 0, STOP.length, mTextBounds)
        mWhiteFillPath.moveTo(mWhitePoints[0][0], mWhitePoints[0][1])
        mRedFillPath.moveTo(mRedPoints[0][0], mRedPoints[0][1])
        for (i in 0 until OCTAGON_SIZE) {
            mWhiteFillPath.lineTo(mWhitePoints[i][0], mWhitePoints[i][1])
            mRedFillPath.lineTo(mRedPoints[i][0], mRedPoints[i][1])
        }
        canvas.drawPath(mWhiteFillPath, mWhitePaint)
        canvas.drawPath(mRedFillPath, mRedPaint)
        canvas.drawText(
                STOP, mSignHalfWidth.toFloat(), mSignHalfWidth + mTextBounds.height() / 2.0f, mTextPaint
        )
    }

    /**
     * This method calculates points coordinates that are used for the octagons plotting.
     */
    private fun calculateHexagonPoints() {
        var angleGrad = (45 / 2.0f).toDouble()
        var angleRad: Double
        var cos: Double
        var sin: Double
        var xWhite: Float
        var yWhite: Float
        var xRed: Float
        var yRed: Float
        val redDelta = 15
        for (i in 0 until OCTAGON_SIZE) {
            angleGrad += 45.0
            angleRad = angleGrad * PI_DIVIDED_BY_180
            cos = cos(angleRad)
            sin = sin(angleRad)
            xWhite = (mSignHalfWidth + mSignHalfWidth * cos).toFloat()
            yWhite = (mSignHalfWidth + mSignHalfWidth * sin).toFloat()
            xRed = (mSignHalfWidth + (mSignHalfWidth - redDelta) * cos).toFloat()
            yRed = (mSignHalfWidth + (mSignHalfWidth - redDelta) * sin).toFloat()
            mWhitePoints[i][0] = xWhite
            mWhitePoints[i][1] = yWhite
            mRedPoints[i][0] = xRed
            mRedPoints[i][1] = yRed
        }
    }

    companion object {
        /**
         * Text size.
         */
        private const val TEXT_SIZE_LARGE = 50f
        private const val OCTAGON_SIZE = 8
        private const val PI_DIVIDED_BY_180 = 0.017453292519943295
        private const val STOP = "STOP"
    }

    init {
        mRedPaint.strokeWidth = 5f
        mRedPaint.color = Color.RED
        mRedPaint.style = Paint.Style.FILL
        mWhitePaint.strokeWidth = 5f
        mWhitePaint.color = Color.WHITE
        mWhitePaint.style = Paint.Style.FILL
        mTextPaint.color = Color.WHITE
        mTextPaint.isAntiAlias = true
        mRedPaint.isAntiAlias = true
        mWhitePaint.isAntiAlias = true
        val metrics = resources.displayMetrics
        var mDensity = metrics.density

        // TODO : Ugly workaround for the devices with more then hdpi,
        //        such ones may have density 4.0
        if (mDensity > 2) {
            mDensity = 2f
        }
        mTextPaint.textSize = TEXT_SIZE_LARGE * mDensity
        mTextPaint.typeface = Typeface.DEFAULT_BOLD
        mTextPaint.textAlign = Paint.Align.CENTER
        mTextBounds = Rect()
    }
}