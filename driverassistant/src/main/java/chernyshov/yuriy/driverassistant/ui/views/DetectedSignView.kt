/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.ViewGroup

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 05.06.14
 * Time: 22:53
 *
 *
 * This class draws detected value of the speed limit road sign.
 */
class DetectedSignView(context: Context?, attrs: AttributeSet?) : ViewGroup(context, attrs) {

    private var mWidth = 0
    private var mHeight = 0
    private var mRects: Array<Rect>? = null
    private val mRedPaint: Paint
    private val mBackgroundPaint: Paint
    private val mBackgroundRect: Rect

    fun setWidth(value: Int) {
        mWidth = value
    }

    fun setHeight(value: Int) {
        mHeight = value
    }

    fun setRects(value: Array<Rect>?) {
        mRects = value
        if (mRects != null) {
            invalidate()
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        mBackgroundRect[0, 0, mWidth] = mHeight
        canvas.drawRect(mBackgroundRect, mBackgroundPaint)
        if (mRects != null) {
            for (rect in mRects!!) {
                canvas.drawRect(rect, mRedPaint)
            }
        }
    }

    companion object {
        private val LOG_TAG = DetectedSignView::class.java.simpleName
    }

    init {
        setWillNotDraw(false)
        mBackgroundRect = Rect()
        mRedPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mBackgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.DITHER_FLAG)
        mRedPaint.color = Color.RED
        mBackgroundPaint.color = Color.TRANSPARENT
    }
}
