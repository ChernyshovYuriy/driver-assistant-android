/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.sensors

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import chernyshov.yuriy.driverassistant.utils.AppLogger
import java.lang.ref.WeakReference

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 19/02/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
class LightSensorManager {

    interface Listener {
        fun onChanged(lightQuantity: Float)
    }

    private var mSensorManager: SensorManager? = null
    private var mLightSensor: Sensor? = null
    private var mListener: SensorEventListener? = null
    private var mIsRegistered = false
    private lateinit var mExternalListener: Listener

    fun init(context: Context): Boolean {
        // Obtain references to the SensorManager and the Light Sensor
        mSensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        if (mSensorManager == null) {
            return false
        }
        mLightSensor = mSensorManager!!.getDefaultSensor(Sensor.TYPE_LIGHT)
        if (mLightSensor == null) {
            return false
        }
        val lightSensors = mSensorManager!!.getSensorList(Sensor.TYPE_LIGHT)
        if (lightSensors != null) {
            AppLogger.d("Light sensors:")
            for (sensor in lightSensors) {
                AppLogger.d(" - $sensor")
            }
        }
        mListener = SensorEventListenerImpl(this)
        return true
    }

    fun setExternalListener(value: Listener) {
        mExternalListener = value
    }

    val isInit: Boolean
        get() = mLightSensor != null

    fun register() {
        if (mLightSensor == null) {
            AppLogger.w("Light Sensor is not supported")
            return
        }
        if (mIsRegistered) {
            return
        }
        mIsRegistered = true

        // Register the listener with the light sensor -- choosing
        // one of the SensorManager.SENSOR_DELAY_* constants.
        mSensorManager!!.registerListener(mListener, mLightSensor, SensorManager.SENSOR_DELAY_UI)
    }

    fun unregister() {
        if (mLightSensor == null) {
            AppLogger.w("Light Sensor is not supported")
            return
        }
        if (!mIsRegistered) {
            return
        }
        mIsRegistered = false
        mSensorManager!!.unregisterListener(mListener, mLightSensor)
    }

    private class SensorEventListenerImpl(reference: LightSensorManager) : SensorEventListener {

        private val mReference = WeakReference(reference)

        override fun onSensorChanged(event: SensorEvent) {
            val value = event.values[0]
            AppLogger.d("Light Sensor value:$value")
            val manager = mReference.get() ?: return
            manager.mExternalListener.onChanged(value)
        }

        override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    }
}
