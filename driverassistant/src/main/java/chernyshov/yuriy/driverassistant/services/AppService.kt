/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.services

import android.app.Activity
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.location.Location
import android.os.Binder
import android.os.IBinder
import android.os.SystemClock
import android.text.TextUtils
import android.util.SparseArray
import androidx.annotation.MainThread
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.core.AbstractCascadeProcessor
import chernyshov.yuriy.driverassistant.core.EllipseRedCascadeProcessor
import chernyshov.yuriy.driverassistant.core.ImageProcessingCallbackImpl
import chernyshov.yuriy.driverassistant.core.OCRDetectorProcessor
import chernyshov.yuriy.driverassistant.core.OCRDetectorProcessorListener
import chernyshov.yuriy.driverassistant.core.SpeedLimitCanadaCascadeProcessor
import chernyshov.yuriy.driverassistant.core.SpeedLimitEUCascadeProcessor
import chernyshov.yuriy.driverassistant.core.StopCascadeProcessor
import chernyshov.yuriy.driverassistant.core.VideoFrameProcessor
import chernyshov.yuriy.driverassistant.core.detection.DetectionCallback
import chernyshov.yuriy.driverassistant.core.detection.DetectionResult
import chernyshov.yuriy.driverassistant.core.detection.RoadSign
import chernyshov.yuriy.driverassistant.preferences.AppPreferenceManager
import chernyshov.yuriy.driverassistant.preferences.CameraPreferenceManager
import chernyshov.yuriy.driverassistant.preferences.GeneralPreferencesManager
import chernyshov.yuriy.driverassistant.preferences.RecognitionPreferenceManager
import chernyshov.yuriy.driverassistant.preferences.SpeedLimitPreferenceManager
import chernyshov.yuriy.driverassistant.preferences.TTSPreferenceManager
import chernyshov.yuriy.driverassistant.services.AppService.SpeedListener.ExceedLevel
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.AppUtils.parseStringToInteger
import chernyshov.yuriy.driverassistant.utils.Constants
import chernyshov.yuriy.driverassistant.utils.SpeedValueUtils
import com.google.android.gms.vision.Frame
import com.google.android.gms.vision.text.TextRecognizer
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference
import java.util.*
import kotlin.math.cos
import kotlin.math.sin

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 01.06.14
 * Time: 16:21
 *
 *
 * This is the main application service that handles different libraries and non UI functionality,
 * such as Optical Character Recognition (OCR) library or Preferences.
 */
class AppService : Service() {

    interface SpeedListener {
        enum class ExceedLevel {
            NONE, MID, HIGH
        }

        /**
         * Call client when speed value is updated, in km/H.
         *
         * @param speed Speed in km/H.
         */
        fun onGpsSpeed(speed: String)
        fun onSpeedExceed(level: ExceedLevel)
    }

    companion object {
        /**
         * Array of the tabulated values of Sines.
         */
        private val SIN_TABLE = SparseArray<Double>()

        /**
         * Array of the tabulated values of Cosines.
         */
        private val COS_TABLE = SparseArray<Double>()

        /**
         * Array of degrees to use to rotate detected sign in order to compensate sign distortion.
         */
        private val ROTATION_DEGREES = intArrayOf(-5, 0, 5)
        private val CLASS_NAME = AppService::class.java.simpleName

        /**
         * Factory method to create an Intent.
         *
         * @param context Context of the callee.
         * @return Instance of the [android.content.Intent]
         */
        fun createIntent(context: Context?): Intent {
            return Intent(context, AppService::class.java)
        }

        init {
            // Tabulate values of Sin and Cos.
            for (degree in ROTATION_DEGREES) {
                SIN_TABLE.put(degree, sin(Math.toRadians(degree.toDouble())))
                COS_TABLE.put(degree, cos(Math.toRadians(degree.toDouble())))
            }
        }
    }

    private lateinit var mTextRecognizer: TextRecognizer
    private lateinit var mOcrDetectorProcessor: OCRDetectorProcessor
    private var mCurrentGpsSpeedValue = 0
    private var mCurrentDetectedSpeedValue = 0

    /**
     * Binder given to clients
     */
    private val mBinder = AppServiceBinder(this)
    private lateinit var mGeneralPreferencesManager: GeneralPreferencesManager
    private lateinit var mTTSPreferenceManager: TTSPreferenceManager
    private lateinit var mCameraPreferenceManager: CameraPreferenceManager
    private lateinit var mSpeedLimitPreferenceManager: SpeedLimitPreferenceManager
    private lateinit var mRecognitionPreferenceManager: RecognitionPreferenceManager

    private var mScope = CoroutineScope(Dispatchers.IO)

    /**
     * Collection of the Preference Managers.
     */
    private val mPreferenceManagers = ArrayList<AppPreferenceManager>()
    private lateinit var mImageProcessingCallback: ImageProcessingCallbackImpl
    private var mVideoFrameProcessor: VideoFrameProcessor? = null
    private val mPreferenceChangeListener = SharedPreferencesChangeListener(this)
    private lateinit var mLocationClient: LocationClient
    private var mSpeedListener: SpeedListener? = null
    private var mIsResumed = false

    override fun onBind(intent: Intent): IBinder {
        AppLogger.i("$CLASS_NAME on bind, intent:$intent")
        return mBinder
    }

    override fun onCreate() {
        super.onCreate()
        AppLogger.i("$CLASS_NAME on create")
        val context = applicationContext
        mImageProcessingCallback = ImageProcessingCallbackImpl(context)
        mGeneralPreferencesManager = GeneralPreferencesManager(context)
        mTTSPreferenceManager = TTSPreferenceManager(context)
        mCameraPreferenceManager = CameraPreferenceManager(context)
        mSpeedLimitPreferenceManager = SpeedLimitPreferenceManager(context)
        mRecognitionPreferenceManager = RecognitionPreferenceManager(context)
        mPreferenceManagers.add(mGeneralPreferencesManager)
        mPreferenceManagers.add(mTTSPreferenceManager)
        mPreferenceManagers.add(mCameraPreferenceManager)
        mPreferenceManagers.add(mSpeedLimitPreferenceManager)
        mPreferenceManagers.add(mRecognitionPreferenceManager)
        mTextRecognizer = TextRecognizer.Builder(context).build()
        mOcrDetectorProcessor = OCRDetectorProcessor()
        mTextRecognizer.setProcessor(mOcrDetectorProcessor)
        mLocationClient = LocationClient(
            context,
            object : LocationClient.Listener {
                override fun onLocationUpdates(location: Location?) {
                    AppLogger.d("$CLASS_NAME location:$location")
                    if (location == null) {
                        return
                    }
                    mCurrentGpsSpeedValue = SpeedValueUtils.metersSecToKmHour(location.speed)
                    analyzeSpeed()
                    if (mSpeedListener == null) {
                        return
                    }
                    val speedStr = String.format(Locale.US, "%d", mCurrentGpsSpeedValue)
                    mSpeedListener!!.onGpsSpeed(speedStr)
                }
            }
        )
        mScope.launch {
            for (manager in mPreferenceManagers) {
                manager.init(mPreferenceChangeListener)
            }
            updateSaveDetectedShape()
            val cascadeProcessors = mutableListOf<AbstractCascadeProcessor>()
            if (isRecognizeStopSigns) {
                cascadeProcessors.add(StopCascadeProcessor())
                AppLogger.d("$CLASS_NAME ${StopCascadeProcessor::class.java.simpleName} added")
            }
            if (isRecognizeRedEllipse) {
                cascadeProcessors.add(EllipseRedCascadeProcessor())
                AppLogger.d("$CLASS_NAME ${EllipseRedCascadeProcessor::class.java.simpleName} added")
            }
            if (isRecognizeSpeedLimit) {
                if (isSpeedLimitEuVariant) {
                    cascadeProcessors.add(SpeedLimitEUCascadeProcessor())
                    AppLogger.d("$CLASS_NAME ${SpeedLimitEUCascadeProcessor::class.java.simpleName} added")
                } else {
                    cascadeProcessors.add(SpeedLimitCanadaCascadeProcessor())
                    AppLogger.d("$CLASS_NAME ${SpeedLimitCanadaCascadeProcessor::class.java.simpleName} added")
                }
            }
            for (processor in cascadeProcessors) {
                processor.init(context)
            }
            mVideoFrameProcessor = VideoFrameProcessor(
                cascadeProcessors.toTypedArray(), mImageProcessingCallback
            )
            mVideoFrameProcessor?.init(Constants.PREVIEW_W, Constants.PREVIEW_H)
        }
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        AppLogger.i("$CLASS_NAME on start, intent:$intent flags:$flags startId:$startId")
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onRebind(intent: Intent) {
        super.onRebind(intent)
        AppLogger.i("$CLASS_NAME on rebind, intent:$intent")
    }

    override fun onUnbind(intent: Intent): Boolean {
        AppLogger.i("$CLASS_NAME on unbind, intent:$intent")
        return super.onUnbind(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        AppLogger.i("$CLASS_NAME on destroy")
        mScope.cancel("Cancel on destroy")
        for (manager in mPreferenceManagers) {
            manager.close()
        }
        mTextRecognizer.release()
        mVideoFrameProcessor?.stop()
    }

    @MainThread
    fun onPause(activity: Activity) {
        mIsResumed = false
        mCurrentDetectedSpeedValue = 0
        mCurrentGpsSpeedValue = 0
        mLocationClient.stopLocationUpdates(activity)
    }

    @MainThread
    fun onResume(activity: Activity) {
        if (mIsResumed) {
            return
        }
        mIsResumed = true
        mLocationClient.startLocationUpdates(activity)
    }

    /**
     * @param data
     */
    fun onVideoData(data: ByteArray) {
        mVideoFrameProcessor?.update(data)
    }

    /**
     * @param callback
     */
    fun setDetectionCallback(callback: DetectionCallback) {
        mImageProcessingCallback.setDetectionCallback(callback)
    }

    /**
     * @param listener
     */
    fun setSpeedListener(listener: SpeedListener) {
        mSpeedListener = listener
    }

    /**
     *
     */
    private fun updateSaveDetectedShape() {
        mImageProcessingCallback.setSaveDetectedShape(isSaveDetectedShape)
    }

    /**
     * @param width
     * @param height
     */
    fun initVideoFrameProcessor(width: Int, height: Int) {
        if (mVideoFrameProcessor!!.isInit) {
            mVideoFrameProcessor?.deInit()
        }
        mVideoFrameProcessor?.init(width, height)
    }

    fun recognizeText(roadSign: RoadSign, detectedValue: DetectionResult) {
        val width = detectedValue.width
        val height = detectedValue.height
        if (width <= 0 || height <= 0) {
            return
        }

        // Create bitmap
        val detectedBitmap = Bitmap.createBitmap(
            width, height, Bitmap.Config.ARGB_8888
        )
        val data = IntArray(detectedValue.data.size)
        for (i in data.indices) {
            data[i] = detectedValue.data[i].toInt()
        }

        // Set the pixels
        detectedBitmap.setPixels(data, 0, width, 0, 0, width, height)

        val rotation = 0
        val pendingTime = SystemClock.elapsedRealtime()
        val pendingFrameId = roadSign.ordinal
        val frame = Frame.Builder()
            .setBitmap(detectedBitmap)
            .setId(pendingFrameId)
            .setTimestampMillis(pendingTime)
            .setRotation(rotation)
            .build()
        mTextRecognizer.receiveFrame(frame)
    }

    fun setOcrDetectorProcessorListener(value: OCRDetectorProcessorListener) {
        mOcrDetectorProcessor.setListener(OCRDetectorProcessorListenerProxy(value))
    }

    /**
     * @return Value of the Camera Zoom.
     */
    val cameraZoom: Int
        get() {
            val zoom = parseStringToInteger(
                mCameraPreferenceManager.preferences.getString(
                    getString( // TODO: Fix this, must be set in preferences manager only
                        R.string.pref_key_camera_zoom
                    ), "30"
                )!!
            )
            AppLogger.d("Get Zoom:$zoom")
            return zoom
        }

    /**
     * @return True if it is necessary to save each detected shape. False - otherwise.
     */
    private val isSaveDetectedShape: Boolean
        get() = mGeneralPreferencesManager.preferences
            .getBoolean(getString(R.string.pref_key_save_detected_shape), false)

    /**
     * Set True if Initial Warning Dialog has been shown before. False - otherwise.
     */
    var isInitWarningDialogShown: Boolean
        get() = mGeneralPreferencesManager.preferences
            .getBoolean(getString(R.string.pref_key_is_init_warning_shown), false)
        set(value) {
            mGeneralPreferencesManager.commitPreferenceBoolean(
                getString(R.string.pref_key_is_init_warning_shown), value
            )
        }
    val isDynamicScreenBrightness: Boolean
        get() = mGeneralPreferencesManager.preferences
            .getBoolean(getString(R.string.pref_key_dynamic_screen_brightness), false)

    /**
     * @return True if TTS Engine is installed on the device. False - otherwise.
     */
    val isTTSEngineAvailable: Boolean
        get() = mTTSPreferenceManager.preferences.getBoolean(getString(R.string.pref_key_tts_enabled), false)

    /**
     * Set True if TTS Engine is installed on the device. False - otherwise.
     */
    var isTTSEngineEnabled: Boolean
        get() = mTTSPreferenceManager.preferences.getBoolean(getString(R.string.pref_key_tts_enabled), false)
        set(value) {
            mScope.launch {
                mTTSPreferenceManager.commitPreferenceBoolean(getString(R.string.pref_key_tts_enabled), value)
            }
        }

    /**
     * @return Whether Speed Limit Sign should be processed as EU standard sign.
     */
    private val isSpeedLimitEuVariant: Boolean
        get() = mSpeedLimitPreferenceManager.preferences
            .getBoolean(getString(R.string.pref_key_speed_limit_eu_variant), true)

    /**
     * Set True if Speed Limit variant has been processed for the first application launch.
     * False - otherwise.
     */
    var isSpeedLimitVariantInit: Boolean
        get() = mGeneralPreferencesManager.preferences
            .getBoolean(getString(R.string.pref_key_is_speed_limit_variant_init), false)
        set(value) {
            mScope.launch {
                mGeneralPreferencesManager.commitPreferenceBoolean(
                    getString(R.string.pref_key_is_speed_limit_variant_init), value
                )
            }
        }
    private val isRecognizeSpeedLimit: Boolean
        get() = mRecognitionPreferenceManager.preferences
            .getBoolean(getString(R.string.pref_key_recognize_speed_limit), true)
    private val isRecognizeStopSigns: Boolean
        get() = mRecognitionPreferenceManager.preferences
            .getBoolean(getString(R.string.pref_key_recognize_stop), true)
    private val isRecognizeRedEllipse: Boolean
        get() = mRecognitionPreferenceManager.preferences
            .getBoolean(getString(R.string.pref_key_recognize_red_ellipse), false)

    private fun analyzeSpeed() {
        AppLogger.d(
            CLASS_NAME + " analyze speed, gps: " + mCurrentGpsSpeedValue
                    + " max:" + mCurrentDetectedSpeedValue + " listener:" + mSpeedListener
        )
        if (mSpeedListener == null) {
            return
        }
        if (mCurrentDetectedSpeedValue == 0) {
            mSpeedListener!!.onSpeedExceed(ExceedLevel.NONE)
            return
        }
        if (mCurrentGpsSpeedValue <= 5) {
            mSpeedListener!!.onSpeedExceed(ExceedLevel.NONE)
            return
        }
        if (mCurrentGpsSpeedValue > mCurrentDetectedSpeedValue
            && mCurrentGpsSpeedValue < mCurrentDetectedSpeedValue + 10
        ) {
            mSpeedListener!!.onSpeedExceed(ExceedLevel.MID)
        }
        if (mCurrentGpsSpeedValue >= mCurrentDetectedSpeedValue + 10) {
            mSpeedListener!!.onSpeedExceed(ExceedLevel.HIGH)
        }
        if (mCurrentGpsSpeedValue <= mCurrentDetectedSpeedValue) {
            mSpeedListener!!.onSpeedExceed(ExceedLevel.NONE)
        }
    }

    /**
     * Class used for the client Binder. Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    class AppServiceBinder(reference: AppService) : Binder() {

        /**
         * Reference to the enclosing class.
         */
        private val mReference: WeakReference<AppService> = WeakReference(reference)

        /**
         * Returns the reference to the OCR Service.
         *
         * @return Reference to the OCR Service.
         */
        val service: AppService?
            get() = mReference.get()

    }

    private class SharedPreferencesChangeListener(appService: AppService) :
        SharedPreferences.OnSharedPreferenceChangeListener {

        private val mAppService: WeakReference<AppService> = WeakReference(appService)

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
            AppLogger.d("Preference " + sharedPreferences.javaClass.simpleName + " changed, key:" + key)
            val appService = mAppService.get() ?: return
            if (TextUtils.equals(key, appService.getString(R.string.pref_key_save_detected_shape))) {
                appService.updateSaveDetectedShape()
            }
        }
    }

    /**
     * Proxy class to handle detected value. It will provide valid value only to the original client.
     */
    private inner class OCRDetectorProcessorListenerProxy(private val mListener: OCRDetectorProcessorListener) :
        OCRDetectorProcessorListener {

        override fun onDetect(roadSign: RoadSign, detectedText: String) {
            var detectedTextCpy = detectedText
            AppLogger.d("$CLASS_NAME On Detect sign:$roadSign, value:$detectedTextCpy")
            when (roadSign) {
                RoadSign.SPEED_LIMIT_CANADA,
                RoadSign.SPEED_LIMIT_EU -> {
                    val normalizedText = SpeedValueUtils.extractSpeedValue(
                        detectedTextCpy.trim { it <= ' ' }
                    )
                    if (SpeedValueUtils.validateSpeedValue(normalizedText)) {
                        detectedTextCpy = normalizedText
                        mCurrentDetectedSpeedValue = normalizedText.toInt()
                        analyzeSpeed()
                    } else {
                        // Return from method if speed value is invalid.
                        return
                    }
                }
            }
            mListener.onDetect(roadSign, detectedTextCpy)
        }
    }
}
