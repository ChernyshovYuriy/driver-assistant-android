/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 11/6/14
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * This is the helper class to hold all necessary constant used by application.
 */
object Constants {

    const val PREVIEW_W = 1280
    const val PREVIEW_H = 720

    /**
     * YUV data array length
     * 3110400 for the 1920 x 1080
     * 1382400 for the 1280 x 720
     * 460800  for the 640  x 480
     *
     * Explanation
     * For 1 frame
     * To extract Y-component : read 640*480 bytes
     * To extract Cb-component: read 640*480/4 bytes
     * To extract Cr-component: r 640*480/4 bytes
     * The size for 1 frame is 640*480*3/2=460800 bytes
     *
     */
    const val PREVIEW_YUV_TYPE_X = 1382400
    const val PREVIEW_MIN_W = 32
    const val PREVIEW_MIN_H = 24
    const val DETECTED_CONTOUR_W = 90
    const val DETECTED_CONTOUR_H = 90

    // These are most likely minimum values that feature detection algorithm can access.
    // It is still a room to lower those and increase recognition time,
    // however it decrease accuracy as well.
    const val TRAIN_INPUT_W = 50
    const val TRAIN_INPUT_H = 50

    /**
     * Mask to measure byte value.
     */
    const val BYTE_MASK = 0xFF

    /**
     * Value of the Black color.
     */
    const val BLACK_COLOR = -0x1000000

    /**
     * Value of the White color.
     */
    const val WHITE_COLOR = -0x1

    object ActivityResultCode {
        const val TTS_CHECK_CODE = 100
    }

    /*public static class HSV {

        public static final int HUE_MAX = 179;
        public static final int SATURATION_MAX = 255;
        public static final int VALUE_MAX = 255;
    }*/
    object NHSL {
        const val HUE_MIN = 15
        const val HUE_MAX = 240
        const val SATURATION_MIN = 25
    }
}