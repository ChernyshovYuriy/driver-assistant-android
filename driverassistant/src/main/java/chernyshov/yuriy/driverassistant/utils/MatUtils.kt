/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 24/12/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
object MatUtils {

    private val SIZE_RESIZE = Size()

    @JvmStatic
    fun resizeImage(img: Mat, size: Size): Mat {
        val img_width = img.cols()
        val img_height = img.rows()
        val ratio: Double
        SIZE_RESIZE.width = size.width
        SIZE_RESIZE.height = size.height
        if (img_width > img_height) {
            ratio = size.width * 1.0 / img_width
            SIZE_RESIZE.height = img_height * ratio
        } else {
            ratio = size.height * 1.0 / img_height
            SIZE_RESIZE.width = img_width * ratio
        }
        val img_resized = Mat(SIZE_RESIZE, img.type())
        AppLogger.d("Resize " + img_width + "x" + img_height + " to " + SIZE_RESIZE)
        Imgproc.resize(img, img_resized, SIZE_RESIZE, 0.0, 0.0, Imgproc.INTER_LANCZOS4)
        return img_resized
    }
}
