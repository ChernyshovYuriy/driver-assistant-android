/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

import android.text.TextUtils
import java.util.*

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 23/12/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
object SpeedValueUtils {
    /**
     * Map of the valid values of the Speed Limit.
     */
    private val SPEED_VALUES: MutableSet<String> = HashSet()

    /**
     * Map of the valid values of the number characters.
     */
    private val CHARACTERS: MutableSet<Char> = HashSet()

    /**
     * Extract a numerical value from the recognized detectedPixels
     *
     * @param value recognized string
     * @return extracted numerical value
     */
    @JvmStatic
    fun extractSpeedValue(value: String): String {
        if (TextUtils.isEmpty(value)) {
            return ""
        }
        val stringBuilder = StringBuilder()
        for (character in value.toCharArray()) {
            if (CHARACTERS.contains(character)) {
                stringBuilder.append(character)
            }
        }
        return stringBuilder.toString()
    }

    /**
     * Validates extracted speed value to meet real values of the speed limit signs
     *
     * @param value value to be validated
     * @return true if input detectedPixels match criteria, false - otherwise
     */
    @JvmStatic
    fun validateSpeedValue(value: String?): Boolean {
        return value != null && value != "" && SPEED_VALUES.contains(value)
    }

    /**
     *
     * @param value
     * @return
     */
    @JvmStatic
    fun metersSecToKmHour(value: Float): Int {
        return (value * 3600 / 1000.0f).toInt()
    }

    /**
     * Fill the Map of the valid values of the Speed Limit
     */
    init {
        SPEED_VALUES.add("5")
        SPEED_VALUES.add("10")
        SPEED_VALUES.add("15")
        SPEED_VALUES.add("20")
        SPEED_VALUES.add("25")
        SPEED_VALUES.add("30")
        SPEED_VALUES.add("35")
        SPEED_VALUES.add("40")
        SPEED_VALUES.add("45")
        SPEED_VALUES.add("50")
        SPEED_VALUES.add("55")
        SPEED_VALUES.add("60")
        SPEED_VALUES.add("65")
        SPEED_VALUES.add("70")
        SPEED_VALUES.add("75")
        SPEED_VALUES.add("80")
        SPEED_VALUES.add("85")
        SPEED_VALUES.add("90")
        SPEED_VALUES.add("95")
        SPEED_VALUES.add("100")
        SPEED_VALUES.add("105")
        SPEED_VALUES.add("110")
        SPEED_VALUES.add("115")
        SPEED_VALUES.add("120")
        SPEED_VALUES.add("125")
        SPEED_VALUES.add("130")
        CHARACTERS.add('0')
        CHARACTERS.add('1')
        CHARACTERS.add('2')
        CHARACTERS.add('3')
        CHARACTERS.add('4')
        CHARACTERS.add('5')
        CHARACTERS.add('6')
        CHARACTERS.add('7')
        CHARACTERS.add('8')
        CHARACTERS.add('9')
    }
}
