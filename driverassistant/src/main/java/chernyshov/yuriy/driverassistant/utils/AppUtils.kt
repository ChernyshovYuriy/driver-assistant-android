/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

import android.annotation.TargetApi
import android.content.Context
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.os.Looper
import android.telephony.TelephonyManager
import android.util.DisplayMetrics
import java.io.File
import java.math.BigDecimal
import java.net.URISyntaxException
import java.util.*

object AppUtils {

    private val CLASS_NAME = AppUtils::class.java.simpleName

    @JvmStatic
    fun getExternalStorageDir(context: Context): String? {
        val externalDir = getExternalFilesDirAPI8(context, null)
        return externalDir?.absolutePath
    }

    @TargetApi(8)
    private fun getExternalFilesDirAPI8(context: Context, type: String?): File? {
        return context.getExternalFilesDir(type)
    }

    fun getApplicationVersionName(context: Context): String {
        val packageInfo = getPackageInfo(context)
        return if (packageInfo != null) {
            packageInfo.versionName
        } else {
            AppLogger.w("Can't get application version")
            "?"
        }
    }

    fun getApplicationVersionCode(context: Context): Int {
        val packageInfo = getPackageInfo(context)
        return if (packageInfo != null) {
            packageInfo.versionCode
        } else {
            AppLogger.w("Can't get application code")
            0
        }
    }

    /**
     * @return PackageInfo for the current application or null if the PackageManager could not be
     * contacted.
     */
    private fun getPackageInfo(context: Context): PackageInfo? {
        val pm = context.packageManager
        if (pm == null) {
            AppLogger.w("Package manager is NULL")
            return null
        }
        var packageName = ""
        return try {
            packageName = context.packageName
            pm.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            AppLogger.e("Failed to find PackageInfo : $packageName")
            null
        } catch (e: RuntimeException) {
            // To catch RuntimeException("Package manager has died") that can occur on some
            // version of Android,
            // when the remote PackageManager is unavailable. I suspect this sometimes occurs
            // when the App is being reinstalled.
            AppLogger.e("Package manager has died : $packageName")
            null
        } catch (e: Throwable) {
            AppLogger.e("Package manager has Throwable : $e")
            null
        }
    }

    @JvmStatic
    fun externalStorageAvailable(): Boolean {
        val externalStorageAvailable: Boolean
        val externalStorageWriteable: Boolean
        when (Environment.getExternalStorageState()) {
            Environment.MEDIA_MOUNTED ->                 // We can read and write the media
            {
                externalStorageWriteable = true
                externalStorageAvailable = externalStorageWriteable
            }
            Environment.MEDIA_MOUNTED_READ_ONLY -> {
                // We can only read the media
                externalStorageAvailable = true
                externalStorageWriteable = false
            }
            else ->                 // Something else is wrong. It may be one of many other states, but all we need
                //  to know is we can neither read nor write
            {
                externalStorageWriteable = false
                externalStorageAvailable = externalStorageWriteable
            }
        }
        return externalStorageAvailable && externalStorageWriteable
    }

    /**
     * This method allows to parse string value into integer value.
     *
     * @param value String value to parse from.
     * @return Integer value.
     */
    @JvmStatic
    fun parseStringToInteger(value: String): Int {
        var integerValue = 0
        try {
            integerValue = value.toInt()
        } catch (e: NumberFormatException) {
            AppLogger.w("Cannot parse String to Integer:" + e.message)
        }
        return integerValue
    }

    /**
     * Help-method to extract URL of the selected Video file from the URI.
     *
     * @param context Context of the callee.
     * @param uri     Uri to be processed.
     * @return Parsed Url.
     * @throws URISyntaxException
     */
    @Throws(URISyntaxException::class)
    fun getPath(context: Context, uri: Uri): String? {
        if ("content".equals(uri.scheme, ignoreCase = true)) {
            val projection = arrayOf("filePath")
            try {
                context.contentResolver.query(uri, projection, null, null, null).use { cursor ->
                    val column_index = cursor?.getColumnIndexOrThrow("filePath") ?: 0
                    if (cursor != null && cursor.moveToFirst()) {
                        return cursor.getString(column_index)
                    }
                }
            } catch (e: Exception) {
                AppLogger.e("Can not get path:$e")
            }
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
        return null
    }

    /**
     * @param context Context of the callee.
     *
     * @return String representation of the devices density.
     */
    fun getDPIAsString(context: Context): String {
        return when (getDisplayMetrics(context).densityDpi) {
            DisplayMetrics.DENSITY_LOW -> "ldpi"
            DisplayMetrics.DENSITY_MEDIUM -> "mdpi"
            DisplayMetrics.DENSITY_HIGH -> "hdpi"
            DisplayMetrics.DENSITY_XHIGH -> "x-hdpi"
            DisplayMetrics.DENSITY_XXHIGH -> "xx-hdpi"
            DisplayMetrics.DENSITY_XXXHIGH -> "xxx-hdpi"
            else -> "unknown dpi:" + DisplayMetrics.DENSITY_DEFAULT
        }
    }

    /**
     * @param context Context of the callee.
     *
     * @return The logical density of the display.
     */
    fun getDensity(context: Context): Float {
        return getDisplayMetrics(context).density
    }

    /**
     * @param context Context of the callee.
     *
     * @return A scaling factor for fonts displayed on the display.
     */
    fun getScaledDensity(context: Context): Float {
        return getDisplayMetrics(context).scaledDensity
    }

    /**
     *
     * @return
     */
    val isRunningUIThread: Boolean
        get() = Looper.myLooper() == Looper.getMainLooper()

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available).
     *
     * @param context Context reference to get the TelephonyManager instance from.
     * @return country code or null
     */
    fun getUserCountry(context: Context): String? {
        try {
            val tm = context.getSystemService(
                    Context.TELEPHONY_SERVICE
            ) as TelephonyManager
            val simCountry = tm.simCountryIso
            if (simCountry != null && simCountry.length == 2) {
                // SIM country code is available
                return simCountry.toLowerCase(Locale.US)
            } else if (tm.phoneType != TelephonyManager.PHONE_TYPE_CDMA) {
                // device is not 3G (would be unreliable)
                val networkCountry = tm.networkCountryIso
                if (networkCountry != null && networkCountry.length == 2) {
                    // network country code is available
                    return networkCountry.toLowerCase(Locale.US)
                }
            }
        } catch (e: Exception) {
            AppLogger.e(CLASS_NAME + " get User country:" + e.message)
        }
        return null
    }

    /**
     * Converting speed value from the float into the string representation, in km/h.
     *
     * @param value The value of the speed, in meters/second over ground.
     *
     * @return [String] representation of the input value.
     */
    @JvmStatic
    fun speedConverter(value: Float): String {
        val kmPerHour = value * 3600.0f / 1000.0f
        var bigDecimal = BigDecimal(kmPerHour.toDouble().toString())
        bigDecimal = bigDecimal.setScale(1, BigDecimal.ROUND_HALF_UP)
        return bigDecimal.toDouble().toString()
    }

    /**
     * @param context Context of the callee.
     * @return [android.util.DisplayMetrics]
     */
    private fun getDisplayMetrics(context: Context): DisplayMetrics {
        val resources = context.resources
        return resources.displayMetrics
    }
}
