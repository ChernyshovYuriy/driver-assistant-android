/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import android.util.Log
import org.opencv.android.Utils
import org.opencv.core.Mat
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException

/**
 * Created by Chernyshov Yurii
 * At Android Studio
 * On 22/01/17
 * E-Mail: chernyshov.yuriy@gmail.com
 */
object FileUtils {

    /**
     *
     * @param path
     * @return
     */
    @JvmStatic
    fun createFileIfNeeded(path: String): File {
        val file = File(path)
        try {
            val result = file.createNewFile()
        } catch (e: IOException) {
            AnalyticsUtils.logException(
                    FileNotFoundException("File $path not created:${Log.getStackTraceString(e)}")
            )
        }
        return file
    }
    /**
     *
     * @param data
     * @param width
     * @param height
     * @param dirName
     */
    @JvmOverloads
    @JvmStatic
    fun arrayToImage(context: Context,
                     data: ByteArray,
                     width: Int,
                     height: Int,
                     dirName: String,
                     namePrefix: String = "") {
        if (width <= 0 || height <= 0) {
            return
        }
        val dataInt = IntArray(data.size)
        for (i in data.indices) {
            dataInt[i] = data[i].toInt()
        }
        val image = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        image.setPixels(dataInt, 0, width, 0, 0, width, height)
        val dirPath = getExternalStorageDir(context) + "/" + dirName
        saveBitmapToFile(
                context,
                image,
                dirPath,
                namePrefix + (if (TextUtils.isEmpty(namePrefix)) "" else "_")
                        + System.currentTimeMillis() + ".jpg"
        )
    }
    /**
     * This method save provided Bitmap to the specified file.
     *
     * @param bitmap   Bitmap data.
     * @param dirName  Path to the directory.
     */
    @JvmOverloads
    fun saveBitmapToFile(context: Context,
                         bitmap: Bitmap?,
                         dirName: String,
                         fileName: String = System.currentTimeMillis().toString() + ".jpg") {
        var dirName = dirName
        if (bitmap == null) {
            AppLogger.e("Save bitmap to file, bitmap is null")
            return
        }
        val extStorageDir = getExternalStorageDir(context)
        if (extStorageDir != null && !dirName.startsWith(extStorageDir)) {
            dirName = "$extStorageDir/$dirName"
        }

        // Create directory if needed
        createDirIfNeeded(dirName)

        //create a file to write bitmap data
        val file = File("$dirName/$fileName")
        AppLogger.d("Save file:" + file.absolutePath)

        // http://stackoverflow.com/questions/11539657/open-failed-ebusy-device-or-resource-busy
        file.renameTo(file)
        file.delete()

        //Convert bitmap to byte array
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()

        //write the bytes in file
        var fileOutputStream: FileOutputStream? = null
        try {
            fileOutputStream = FileOutputStream(file)
            fileOutputStream.write(byteArray)
        } catch (e: IOException) {
            AppLogger.e("Can not save bitmap:" + Log.getStackTraceString(e))
        } finally {
            try {
                byteArrayOutputStream.flush()
            } catch (e: IOException) {
                /* Ignore */
            }
            try {
                if (fileOutputStream != null) {
                    fileOutputStream.flush()
                    fileOutputStream.close()
                }
            } catch (e: IOException) {
                /* Ignore */
            }
        }
    }

    /**
     * This method creates a directory with given name is such does not exists
     *
     * @param path a path to the directory
     */
    @JvmStatic
    fun createDirIfNeeded(path: String?) {
        val file = File(path)
        if (file.exists() && !file.isDirectory) {
            file.delete()
        }
        if (!file.exists()) {
            file.mkdirs()
        }
    }

    /**
     * This method creates a directory with given name is such does not exists
     *
     * @param path a path to the directory
     */
    fun createFile(path: String?): Boolean {
        var result = false
        val file = File(path)
        if (file.exists() && !file.isDirectory) {
            file.delete()
        }
        if (!file.exists()) {
            try {
                result = file.createNewFile()
            } catch (e: IOException) {
                AppLogger.e("Can not create file:" + Log.getStackTraceString(e))
            }
        }
        return result
    }

    fun getExternalStorageDir(context: Context): String? {
        val externalDir = getExternalFilesDirAPI8(context, null)
        return externalDir?.absolutePath
    }

    fun getScaledDataFromRawFile(context: Context, id: Int,
                                 scaledWidth: Int, scaledHeight: Int): IntArray {
        val scaledBitmap = getScaledBitmapFromRawFile(
                context, id, scaledWidth, scaledHeight
        )
        val data = IntArray(scaledWidth * scaledHeight)
        scaledBitmap.getPixels(
                data, 0, scaledWidth, 0, 0, scaledWidth, scaledHeight
        )
        return data
    }

    /**
     * Return [java.io.File] object legal to call on API 8.
     *
     * @param type The type of files directory to return. May be null for the root of the
     * files directory or one of the following Environment constants for a subdirectory:
     * DIRECTORY_MUSIC, DIRECTORY_PODCASTS, DIRECTORY_RINGTONES, DIRECTORY_ALARMS,
     * DIRECTORY_NOTIFICATIONS, DIRECTORY_PICTURES, or DIRECTORY_MOVIES.
     * @return [java.io.File] object
     */
    @TargetApi(8)
    fun getExternalFilesDirAPI8(context: Context, type: String?): File? {
        return context.getExternalFilesDir(type)
    }

    fun getScaledBitmapFromRawFile(context: Context,
                                   id: Int,
                                   scaledWidth: Int,
                                   scaledHeight: Int): Bitmap {
        val bitmap = BitmapFactory.decodeStream(
                context.resources.openRawResource(id)
        )
        return Bitmap.createScaledBitmap(
                bitmap, scaledWidth, scaledHeight, false
        )
    }

    @JvmOverloads
    @JvmStatic
    fun saveMatToFile(context: Context,
                      mat: Mat,
                      dirName: String,
                      fileName: String = System.currentTimeMillis().toString() + ".jpg") {
        val bitmap = Bitmap.createBitmap(
                mat.cols(), mat.rows(), Bitmap.Config.RGB_565
        )
        Utils.matToBitmap(mat, bitmap)
        saveBitmapToFile(context, bitmap, dirName, fileName)
    }

    /**
     * Load file.
     *
     * @param context Context of application.
     * @param resourceId
     * @param fileName
     * @return Loaded file.
     */
    @JvmStatic
    fun loadFile(context: Context,
                 resourceId: Int,
                 fileName: String?): File {
        val `is` = context.resources.openRawResource(resourceId)
        val filesDir = context.getDir("tmp", Context.MODE_PRIVATE)
        val file = File(filesDir, fileName)
        var os: FileOutputStream? = null
        val buffer = ByteArray(4096)
        var bytesRead: Int
        try {
            os = FileOutputStream(file)
            while (`is`.read(buffer).also { bytesRead = it } != -1) {
                os.write(buffer, 0, bytesRead)
            }
        } catch (e: Exception) {
            AppLogger.e("Can not read file:" + Log.getStackTraceString(e))
        } finally {
            try {
                `is`.close()
                os?.close()
            } catch (e: IOException) {
                /* Ignore */
            }
        }
        val result = filesDir.delete()
        //        AppLogger.d("Temporary file deleted:" + result);
        return file
    }

    /**
     *
     * @param context
     * @return
     */
    @JvmStatic
    fun getFilesDir(context: Context): File {
        return context.filesDir
    }
}
