/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

import android.content.Intent
import android.speech.tts.TextToSpeech

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 11/6/14
 * E-Mail: chernyshov.yuriy@gmail.com
 *
 * This is a helper class to provide factory methods for the various Intents.
 */
object IntentBuilder {

    /**
     * This method creates an Intent to check whether TTS is available on the device.
     *
     * @return Instance of the Intent.
     */
    fun createCheckTTSIntent(): Intent {
        val intent = Intent()
        intent.action = TextToSpeech.Engine.ACTION_CHECK_TTS_DATA
        return intent
    }

    /**
     * This method creates an Intent to install TTS engine.
     *
     * @return Instance of the Intent.
     */
    fun createInstallTTSIntent(): Intent {
        val intent = Intent()
        intent.action = TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA
        return intent
    }

    /**
     * This method creates an Intent to open files browser in order to get Video file.
     *
     * @return Instance of the Intent.
     */
    fun createSelectVideoFileIntent(): Intent {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.type = "video/*"
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        return intent
    }
}
