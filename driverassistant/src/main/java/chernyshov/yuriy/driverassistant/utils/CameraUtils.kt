/*
 * Copyright 2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.utils

import androidx.camera.core.Camera

object CameraUtils {

    const val ZOOM_MIN = 1.0F
    const val ZOOM_MAX = 1.0F

    fun calculateFactor(value: Int, min: Float, max: Float): Float {
        return value * (max - min) / 100
    }

    fun getZoomMinMax(camera: Camera?): Pair<Float, Float> {
        if (camera == null) {
            AppLogger.w("Camera is invalid")
            return Pair(ZOOM_MIN, ZOOM_MAX)
        }
        val info = camera.cameraInfo
        val zoomState = info.zoomState.value
        if (zoomState == null) {
            AppLogger.w("Camera's zoom state is invalid")
            return Pair(ZOOM_MIN, ZOOM_MAX)
        }
        return Pair(zoomState.minZoomRatio, zoomState.maxZoomRatio)
    }

    fun dumpCameraInfo(camera: Camera?) {
        if (camera == null) {
            AppLogger.w("Camera is invalid")
            return
        }
        val info = camera.cameraInfo
        val zoomState = info.zoomState.value
        if (zoomState != null) {
            val zoomRatio = zoomState.zoomRatio
            val maxZoomRatio = zoomState.maxZoomRatio
            val minZoomRatio = zoomState.minZoomRatio
            val linearZoom = zoomState.linearZoom
            AppLogger.i(
                    "Camera params:{zoomRatio:$zoomRatio, zoomMax:$maxZoomRatio, zoomMin:$minZoomRatio, " +
                            "linearZoom:$linearZoom}"
            )
        } else {
            AppLogger.w("Camera's zoom state is invalid")
        }
    }
}
