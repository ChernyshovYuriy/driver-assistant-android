/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant

import android.os.Build
import androidx.multidex.MultiDexApplication
import chernyshov.yuriy.driverassistant.utils.AnalyticsUtils
import chernyshov.yuriy.driverassistant.utils.AppLogger
import chernyshov.yuriy.driverassistant.utils.AppUtils
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.google.android.exoplayer2.upstream.TransferListener
import com.google.android.exoplayer2.util.Util
import org.opencv.core.Core

/**
 * Created with Android Studio.
 * User: Yuriy Chernyshov
 * Date: 12/21/13
 * Time: 6:29 PM
 */
class MainApp : MultiDexApplication() {

    private var userAgent: String? = null

    companion object {
        init {
            System.loadLibrary("opencv_java4")
            System.loadLibrary("detection_based_tracker")
        }
    }

    override fun onCreate() {
        super.onCreate()
        val context = applicationContext
        AnalyticsUtils.init()
        AppLogger.initLogger(context)
        userAgent = Util.getUserAgent(context, "ExoPlayerDemo")
        printFirstLogMessage()
    }

    /**
     * Print first log message with summary information about device and application.
     */
    private fun printFirstLogMessage() {
        val context = applicationContext
        val builder = StringBuilder()
        builder.append("\n")
        builder.append("########### Create '")
        builder.append(getString(R.string.app_name))
        builder.append("' Application ###########\n")
        builder.append("- processors: ")
        builder.append(Runtime.getRuntime().availableProcessors())
        builder.append("\n")
        builder.append("- version: ")
        builder.append(AppUtils.getApplicationVersionCode(context))
        builder.append(".")
        builder.append(AppUtils.getApplicationVersionName(context))
        builder.append("\n")
        builder.append("- OS ver: ")
        builder.append(Build.VERSION.RELEASE)
        builder.append("\n")
        builder.append("- API level: ")
        builder.append(Build.VERSION.SDK_INT)
        builder.append("\n")
        builder.append("- DPI: ")
        builder.append(AppUtils.getDPIAsString(this))
        builder.append("\n")
        builder.append("- Density: ")
        builder.append(AppUtils.getDensity(this))
        builder.append("\n")
        builder.append("- Scaling Density: ")
        builder.append(AppUtils.getScaledDensity(this))
        builder.append("\n")
        builder.append("- Country: ")
        builder.append(AppUtils.getUserCountry(context))
        builder.append("\n")
        builder.append("- OpenCV: ")
        builder.append(Core.getVersionString())
        AppLogger.i(builder.toString())
    }

    /** Returns a [DataSource.Factory].  */
    fun buildDataSourceFactory(listener: TransferListener<in DataSource?>?): DataSource.Factory {
        return DefaultDataSourceFactory(this, listener, buildHttpDataSourceFactory(listener))
    }

    /** Returns a [HttpDataSource.Factory].  */
    private fun buildHttpDataSourceFactory(listener: TransferListener<in DataSource?>?): HttpDataSource.Factory {
        return DefaultHttpDataSourceFactory(userAgent, listener)
    }

    fun useExtensionRenderers(): Boolean {
        return true
    }
}
