/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.preferences

import android.content.Context
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.utils.AppLogger.d

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 08.06.14
 * Time: 17:54
 */
class GeneralPreferencesManager(context: Context) : AppPreferenceManager(context) {

    override fun createPreferenceListener(): PreferenceListener {
        return PreferenceListenerImpl(this)
    }

    /**
     * Listener implementation.
     */
    private class PreferenceListenerImpl(reference: AppPreferenceManager) : PreferenceListener(reference) {

        override val watchedPreferenceKeyIds = intArrayOf(
                R.string.pref_key_is_init_warning_shown,
                R.string.pref_key_save_detected_shape,
                R.string.pref_key_is_speed_limit_variant_init,
                R.string.pref_key_dynamic_screen_brightness
        )

        override fun readStoredPreferences() {
            val reference = reference ?: return
            val isInitWarningDialogShown = reference.preferences
                    .getBoolean(reference.getString(R.string.pref_key_is_init_warning_shown), false)
            d("$CLASS_NAME 'Init Warning Dialog' shown:$isInitWarningDialogShown")
            val isSaveDetectedShape = reference.preferences
                    .getBoolean(reference.getString(R.string.pref_key_save_detected_shape), false)
            d("$CLASS_NAME Save detected shape:$isSaveDetectedShape")
            val isSpeedLimitVariantInit = reference.preferences
                    .getBoolean(reference.getString(R.string.pref_key_is_speed_limit_variant_init), false)
            d("$CLASS_NAME Speed Limit variant init:$isSpeedLimitVariantInit")
            val isDynamicLight = reference.preferences
                    .getBoolean(reference.getString(R.string.pref_key_dynamic_screen_brightness), false)
            d("$CLASS_NAME Dynamic screen brightness:$isDynamicLight")
        }
    }

    companion object {
        private val CLASS_NAME = GeneralPreferencesManager::class.java.simpleName
    }
}
