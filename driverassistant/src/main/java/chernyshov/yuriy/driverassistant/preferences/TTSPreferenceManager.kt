/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.preferences

import android.content.Context
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.utils.AppLogger.d

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 11/6/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */
class TTSPreferenceManager(context: Context) : AppPreferenceManager(context) {

    override fun createPreferenceListener(): PreferenceListener {
        return PreferenceListenerImpl(this)
    }

    /**
     * Listener implementation.
     */
    private class PreferenceListenerImpl(reference: AppPreferenceManager) : PreferenceListener(reference) {

        override fun readStoredPreferences() {
            val reference = reference ?: return
            val isTTSEnabled = reference.preferences.getBoolean(
                    reference.getString(R.string.pref_key_tts_enabled), false
            )
            d("$CLASS_NAME TTS Engine enabled:$isTTSEnabled")
        }

        override val watchedPreferenceKeyIds: IntArray
            get() = WATCHED_PREFERENCE_KEY_IDS
    }

    companion object {

        private val CLASS_NAME = TTSPreferenceManager::class.java.simpleName

        private val WATCHED_PREFERENCE_KEY_IDS = intArrayOf(R.string.pref_key_tts_enabled)
    }
}
