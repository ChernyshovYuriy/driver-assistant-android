/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.preferences

import android.content.Context
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.utils.AppLogger.d

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 11/6/14
 * E-Mail: chernyshov.yuriy@gmail.com
 */
class RecognitionPreferenceManager(context: Context) : AppPreferenceManager(context) {

    override fun createPreferenceListener(): PreferenceListener {
        return PreferenceListenerImpl(this)
    }

    /**
     * Listener implementation.
     */
    private class PreferenceListenerImpl (reference: AppPreferenceManager) : PreferenceListener(reference) {

        override val watchedPreferenceKeyIds = intArrayOf(
                R.string.pref_key_recognize_speed_limit,
                R.string.pref_key_recognize_stop,
                R.string.pref_key_recognize_red_ellipse
        )

        override fun readStoredPreferences() {
            val reference = reference ?: return
            val isSpeedLimit = reference.preferences.getBoolean(
                    reference.getString(R.string.pref_key_recognize_speed_limit), true
            )
            val isStopSign = reference.preferences.getBoolean(
                    reference.getString(R.string.pref_key_recognize_stop), true
            )
            val isRedEllipse = reference.preferences.getBoolean(
                    reference.getString(R.string.pref_key_recognize_red_ellipse), false
            )
            d("$CLASS_NAME Recognize Speed Limit:$isSpeedLimit")
            d("$CLASS_NAME Recognize Stop Sign  :$isStopSign")
            d("$CLASS_NAME Recognize Red Ellipse:$isRedEllipse")
        }
    }

    companion object {
        private val CLASS_NAME = RecognitionPreferenceManager::class.java.simpleName
    }
}
