/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.preferences

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.preference.PreferenceManager
import java.lang.ref.WeakReference
import java.util.*

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 08.06.14
 * Time: 17:43
 *
 * Abstract base class that collects functionality common to watching shared
 * preferences for changes and altering running services as a result.
 *
 * The preference listeners for each specific group of preferences can be
 * contained in a subclass, instead of all cluttering up the main activity.
 */
abstract class AppPreferenceManager internal constructor(protected val context: Context) {

    val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val mPreferenceListenerCompositor = SharedPreferencesChangeListenerCompositor()

    /**
     * Initialize preference listener
     */
    fun init(listener: OnSharedPreferenceChangeListener) {
        val preferenceListener = createPreferenceListener()
        mPreferenceListenerCompositor.addListener(listener)
        mPreferenceListenerCompositor.addListener(preferenceListener)
        preferences.registerOnSharedPreferenceChangeListener(mPreferenceListenerCompositor)
        preferenceListener.readStoredPreferences()
    }

    /**
     * Shutdown any running services and stop watching the shared preferences.
     */
    fun close() {
        preferences.unregisterOnSharedPreferenceChangeListener(mPreferenceListenerCompositor)
        mPreferenceListenerCompositor.clear()
    }

    fun getString(id: Int): String {
        return context.getString(id)
    }

    fun commitPreferenceBoolean(key: String?, value: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    /**
     * Return an instance of a PreferenceListener implementation, defined by the
     * subclass.
     */
    protected abstract fun createPreferenceListener(): PreferenceListener

    /**
     *
     */
    abstract class PreferenceListener(reference: AppPreferenceManager) : OnSharedPreferenceChangeListener {

        private val mReference = WeakReference(reference)

        /**
         * Re-read shared preferences and update any running services.
         *
         * This method will be called whenever the value of any of this
         * PreferenceListener's watched preferences changes.
         */
        abstract fun readStoredPreferences()

        /**
         * Return an array of string resource IDs that correspond to the
         * preference keys that should be monitored for changes.
         */
        protected abstract val watchedPreferenceKeyIds: IntArray

        /**
         *
         * @return
         */
        protected val reference: AppPreferenceManager?
            get() = mReference.get()

        /**
         * If any of the watched preference keys changed, trigger a refresh of
         * the service.
         */
        override fun onSharedPreferenceChanged(preferences: SharedPreferences, key: String) {
            val reference = reference ?: return
            for (watchedKeyId in watchedPreferenceKeyIds) {
                if (key == reference.getString(watchedKeyId)) {
                    readStoredPreferences()
                    break
                }
            }
        }
    }

    private class SharedPreferencesChangeListenerCompositor : OnSharedPreferenceChangeListener {

        private val mList = LinkedList<OnSharedPreferenceChangeListener>()

        fun addListener(listener: OnSharedPreferenceChangeListener) {
            mList.add(listener)
        }

        fun clear() {
            mList.clear()
        }

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
            for (listener in mList) {
                listener.onSharedPreferenceChanged(sharedPreferences, key)
            }
        }
    }
}
