/*
 * Copyright 2017-2021 The "Driver Assistant" Project. Author: Chernyshov Yuriy [chernyshov.yuriy@gmail.com]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package chernyshov.yuriy.driverassistant.preferences

import android.content.Context
import chernyshov.yuriy.driverassistant.R
import chernyshov.yuriy.driverassistant.utils.AppLogger.d
import chernyshov.yuriy.driverassistant.utils.AppUtils.parseStringToInteger

/**
 * Created with Android Studio.
 * User: Chernyshov Yuriy
 * Date: 10.06.14
 * Time: 21:59
 */
class CameraPreferenceManager(context: Context) : AppPreferenceManager(context) {

    override fun createPreferenceListener(): PreferenceListener {
        return PreferenceListenerImpl(this)
    }

    /**
     * Listener implementation.
     */
    private class PreferenceListenerImpl(reference: AppPreferenceManager) : PreferenceListener(reference) {

        override val watchedPreferenceKeyIds = intArrayOf(
                R.string.pref_key_camera_zoom
        )

        override fun readStoredPreferences() {
            val reference = reference ?: return
            val cameraZoom = parseStringToInteger(
                    reference.preferences.getString(
                            reference.getString(R.string.pref_key_camera_zoom), DEFAULT_ZOOM.toString()
                    ) ?: DEFAULT_ZOOM.toString()
            )
            d("$CLASS_NAME Camera Zoom:$cameraZoom")
        }
    }

    companion object {

        const val DEFAULT_ZOOM = 30
        private val CLASS_NAME = CameraPreferenceManager::class.java.simpleName
    }
}
