import os
from random import randint
from threading import Lock
from time import sleep

import cv2
import numpy
import numpy as np
from IPython.utils.py3compat import xrange
from PIL import Image, ImageFile
from numpy import random

# Provide here custom path to the image which will be used to preprocess.
src = "../res/drawable-hdpi/no_stopping_100_100.png"
#
preprocessed_img_prefix = "trng_img_05_"
# Destination directory.
dst = "/home/yurii/Pictures/TEST"
# Number of mutations.
num = 500
width = 50
height = 50


def find_coeffs(pa, pb):
    matrix = []
    for p1, p2 in zip(pa, pb):
        matrix.append([p1[0], p1[1], 1, 0, 0, 0, -p2[0] * p1[0], -p2[0] * p1[1]])
        matrix.append([0, 0, 0, p1[0], p1[1], 1, -p2[1] * p1[0], -p2[1] * p1[1]])

    A = numpy.matrix(matrix, dtype=numpy.float)
    B = numpy.array(pb).reshape(8)

    res = numpy.dot(numpy.linalg.inv(A.T * A) * A.T, B)
    return numpy.array(res).reshape(8)


def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    inv_gamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** inv_gamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using the lookup table
    return Image.fromarray(
        cv2.LUT(numpy.array(image), table)
    )


def fake_light(image, tilesize=50):
    image_width, image_height = image.size
    for x in xrange(0, image_width, tilesize):
        for y in xrange(0, image_height, tilesize):
            br = int(255 * (1 - x / float(image_width) * y / float(image_height)))
            tile = Image.new("RGBA", (tilesize, tilesize), (255, 255, 255, 128))
            image.paste((br, br, br), (x, y, x + tilesize, y + tilesize), mask=tile)


def get_coeffs():
    global width, height
    num_of_coeff_variants = 3
    coeff_id = randint(0, num_of_coeff_variants)
    shift = randint(1, 15)
    if coeff_id == 0:
        """
        Transform right side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(0, 0), (width + shift, -shift), (width + shift, height + shift), (0, height)])
    elif coeff_id == 1:
        """
        Transform left side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(-shift, -shift), (width, 0), (width, height), (-shift, height + shift)])
    elif coeff_id == 2:
        """
        Transform top side in perspective
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(-shift, -shift), (width + shift * 2, -shift), (width, height), (0, height)])
    else:
        """
        No transform
        """
        coeffs = find_coeffs(
            [(0, 0), (width, 0), (width, height), (0, height)],
            [(0, 0), (width, 0), (width, height), (0, height)])
    return coeffs


print("Start images transformation ...")
print(" - source    ", src)

lock = Lock()
# Use this to avoid IOError: image file is truncated (N bytes not processed)
ImageFile.LOAD_TRUNCATED_IMAGES = True
img_current_count = 0
for i in range(0, num):
    lock.acquire()
    try:
        img = Image.open(src)
        img = img.resize((width, height), Image.ANTIALIAS)

        bg_img = Image.new('RGB', (width, height), (255, 255, 255))

        img = img.transform(
            (width, height),
            Image.PERSPECTIVE,
            get_coeffs(),
            Image.BICUBIC
        )

        bg_img.paste(img, (0, 0), img)

        img_current_count += 1

        if img_current_count % 3 == 0:
            fake_light(bg_img, randint(1, 10))

        bg_img = adjust_gamma(bg_img, random.uniform(0.2, 1))

        file_name_new = preprocessed_img_prefix + str(img_current_count) + ".png"
        file_name_full = os.path.join(dst, file_name_new)
        bg_img.save(
            file_name_full,
            "PNG", compress_level=0, optimize=False
        )

        sleep(0.01)

        print("Image", img_current_count, "of", num)
    finally:
        lock.release()

print("Images transformed")
