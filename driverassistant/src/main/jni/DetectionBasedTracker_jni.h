/*
 * Copyright 2017-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <jni.h>

#ifndef _Included_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker
#define _Included_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     DetectionBasedTracker
 * Method:    nativeCreateObject
 * Signature: (Ljava/lang/String;F)J
 */
JNIEXPORT jlong JNICALL Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeCreateObject
        (JNIEnv *, jclass, jstring, jint);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeDestroyObject
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeDestroyObject
        (JNIEnv *, jclass, jlong);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeSetObjectSize
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeSetObjectSize
        (JNIEnv *, jclass, jlong, jint);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeDetect
 * Signature: (JJJ)V
 */
JNIEXPORT void JNICALL Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeDetect
        (JNIEnv *, jclass, jlong, jlong, jlong, jfloat);

#ifdef __cplusplus
}
#endif
#endif
