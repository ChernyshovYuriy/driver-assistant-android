/*
 * Copyright 2017-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "DetectionBasedTracker_jni.h"
#include <opencv2/core.hpp>
#include <opencv2/objdetect.hpp>

#include <string>
#include <vector>

#define  LOG_TAG    "DETECTION-BASE-TRACKER"

#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)

using namespace std;
using namespace cv;

class CascadeDetectorAdapter : public DetectionBasedTracker::IDetector {
public:
    explicit CascadeDetectorAdapter(const Ptr<CascadeClassifier>& detector) :
            IDetector(),
            detector(detector) {
        CV_Assert(detector);
    }

    void detect(const Mat &image, vector<Rect> &objects) override {
//        LOGD(
//                "DetectMultiScale, scaleFactor:%f, minNeighbours:%d, minObjSize:%dx%d, maxObjSize:%dx%d",
//                scaleFactor, minNeighbours,
//                minObjSize.width, minObjSize.height,
//                maxObjSize.width, maxObjSize.height
//        );
        detector->detectMultiScale(
                image, objects, scaleFactor, minNeighbours, 0, minObjSize, maxObjSize
        );
    }

    ~CascadeDetectorAdapter() override = default;

private:
    CascadeDetectorAdapter();
    Ptr<CascadeClassifier> detector;
};

CascadeDetectorAdapter::CascadeDetectorAdapter() = default;

struct DetectorAgregator {
    Ptr<CascadeDetectorAdapter> mainDetector;

    explicit DetectorAgregator(Ptr<CascadeDetectorAdapter> &_mainDetector) :
            mainDetector(_mainDetector) {
        CV_Assert(_mainDetector);
        Size maxObjSize;
        maxObjSize.width = 300;
        maxObjSize.height = 300;
        mainDetector->setMaxObjectSize(maxObjSize);
    }
};

#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jlong JNICALL
Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeCreateObject
        (JNIEnv *jenv, jclass, jstring jFileName, jint objectSize) {
    const char *jnamestr = jenv->GetStringUTFChars(jFileName, nullptr);
    string stdFileName(jnamestr);
    jlong result = 0;
    Ptr<CascadeDetectorAdapter> mainDetector = makePtr<CascadeDetectorAdapter>(
            makePtr<CascadeClassifier>(stdFileName)
    );
    result = (jlong) new DetectorAgregator(mainDetector);
    if (objectSize > 0) {
        mainDetector->setMinObjectSize(Size(objectSize, objectSize));
    }
    return result;
}

JNIEXPORT void JNICALL
Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeDestroyObject
        (JNIEnv *jenv, jclass, jlong thiz) {
    if (thiz != 0) {
        delete (DetectorAgregator *) thiz;
    }
}

JNIEXPORT void JNICALL
Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeSetObjectSize
        (JNIEnv *jenv, jclass, jlong thiz, jint objectSize) {
    if (objectSize > 0) {
        ((DetectorAgregator *) thiz)->mainDetector->setMinObjectSize(
                Size(objectSize, objectSize)
        );
    }
}

JNIEXPORT void JNICALL
Java_chernyshov_yuriy_driverassistant_core_DetectionBasedTracker_nativeDetect
        (JNIEnv *jenv, jclass, jlong thiz, jlong imageGray, jlong objects, jfloat scale_factor) {
    vector<Rect> rectObjects;
//    ((DetectorAgregator *) thiz)->mainDetector->setScaleFactor(scale_factor);
    ((DetectorAgregator *) thiz)->mainDetector->detect(*(Mat *) imageGray, rectObjects);
    *((Mat *) objects) = Mat(rectObjects, true);
    rectObjects.clear();
}
#ifdef __cplusplus
}
#endif
