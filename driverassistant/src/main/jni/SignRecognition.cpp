/*
 * Copyright 2017-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "SignRecognition.h"
#include <iostream>
#include "opencv2/opencv.hpp"

#include <android/log.h>
#include <vector>

#define  LOG_TAG    "SIGN-RECOGNITION"

#define  LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

using namespace std;
using namespace cv;
using namespace cv::ml;

static const Size size20(20, 20);
static Size sizeResize;

class SignRecognition {

public:
    SignRecognition ();
    ~SignRecognition ();

    void init();
    void add(Mat &mat, int index);
    int recognize(Mat &number_mat, int type);

private:
    Ptr<SVM> svm;
    int descriptor_size{};
    HOGDescriptor hog;
    vector<float> descriptors;
    Mat response;
    vector<int> train_labels;
    vector<Mat> train_cells;
};

inline static Mat resizeImage(const Mat &img, const Size& size) {
    const int img_width = img.cols;
    if (img_width == 0) {
        throw invalid_argument("Size can not be of 0 width");
    }
    const int img_height = img.rows;
    if (img_height == 0) {
        throw invalid_argument("Size can not be of 0 height");
    }
    double ratio;
    sizeResize.width = size.width;
    sizeResize.height = size.height;
    if (img_width > img_height) {
        ratio = size.width * 1.0 / img_width;
        sizeResize.height = int(img_height * ratio);
    } else {
        ratio = size.height * 1.0 / img_height;
        sizeResize.width = int(img_width * ratio);
    }
    Mat img_resized;
//    cout << "Resize:" << img.cols << "x" << img.rows << " to:" << size.width << "x" << size.height << endl;
    resize(img, img_resized, sizeResize, INTER_LANCZOS4);
    return img_resized;
}

SignRecognition::SignRecognition() = default;

SignRecognition::~SignRecognition() = default;

void SignRecognition::init() {
    hog.winSize = Size(20, 20);
    hog.blockSize = Size(10, 10);
    hog.blockStride = Size(5, 5);
    hog.cellSize = Size(10, 10);
    hog.nbins = 9;
    hog.derivAperture = 1;
    hog.winSigma = -1;
//    hog.histogramNormType = 0;
    hog.L2HysThreshold = 0.2;
    hog.gammaCorrection = false;
    hog.nlevels = 64;
    hog.signedGradient = true;

    LOGD("Train cells : %d", train_cells.size());

    vector<vector<float> > train_hog;
    for (const auto &deskewed_train_cell : train_cells) {
        std::vector<float> localDescriptors;
        hog.compute(deskewed_train_cell, localDescriptors);
        train_hog.push_back(localDescriptors);
    }

    descriptor_size = !train_hog.empty() ? static_cast<int>(train_hog[0].size()) : 0;
    LOGD("Descriptor Size : %d", descriptor_size);

    Mat train_mat(static_cast<int>(train_hog.size()), descriptor_size, CV_32FC1);
    for (int i = 0; i < train_hog.size(); i++) {
        for (int j = 0; j < descriptor_size; j++) {
            train_mat.at<float>(i, j) = train_hog[i][j];
        }
    }

    svm = SVM::create();
    svm->setGamma(0.50625);
    svm->setC(12.5);
    svm->setKernel(SVM::RBF);
    svm->setType(SVM::C_SVC);
    if (!train_hog.empty()) {
        Ptr<TrainData> td = TrainData::create(train_mat, ROW_SAMPLE, train_labels);
        svm->train(td);
        LOGD("Trained by data");
    } else {
        LOGD("Not trained, data empty");
    }

    train_mat.release();
    train_hog.clear();
    train_labels.clear();
    train_cells.clear();
}

void SignRecognition::add(Mat &mat, int index) {
    mat = resizeImage(mat, size20);
    train_cells.push_back(mat);
    train_labels.push_back(index);
}

int SignRecognition::recognize(Mat &number_mat, int type) {
//    const clock_t begin_time = clock();

    number_mat = resizeImage(number_mat, size20);

    vector<float> localDescriptors;
    hog.compute(number_mat, localDescriptors);

    Mat response_mat(static_cast<int>(1), descriptor_size, CV_32FC1);
    for (int j = 0; j < descriptor_size; ++j) {
        response_mat.at<float>(0, j) = localDescriptors[j];
    }

    Mat localResponse;
    svm->predict(response_mat, localResponse);

    const auto value = static_cast<int>(localResponse.at<float>(0, 0));
//    LOGD("Recognized value %d in %f sec", value, (float(clock() - begin_time ) / CLOCKS_PER_SEC));

    localDescriptors.clear();
    response_mat.release();
    localResponse.release();

    return value;
}

#ifdef __cplusplus
extern "C" {
#endif
JNIEXPORT jlong JNICALL
Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeCreateObject
        (JNIEnv *jenv, jclass) {
    return (jlong) new SignRecognition();
}

JNIEXPORT void JNICALL
Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeDestroyObject
        (JNIEnv *jenv, jclass, jlong thiz) {
    if (thiz != 0) {
        delete (SignRecognition *) thiz;
    }
}

JNIEXPORT void JNICALL
Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeAdd
        (JNIEnv *jenv, jclass, jlong thiz, jstring jFileName, jint index) {
    const char *jnamestr = jenv->GetStringUTFChars(jFileName, nullptr);
    string stdFileName(jnamestr);
    Mat mat = imread(stdFileName, IMREAD_GRAYSCALE);
    ((SignRecognition *) thiz)->add(mat, index);
}

JNIEXPORT void JNICALL
Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeInit
        (JNIEnv *jenv, jclass, jlong thiz) {
    ((SignRecognition *) thiz)->init();
}

JNIEXPORT jint JNICALL
Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeRecognize
        (JNIEnv *jenv, jclass, jlong thiz, jlong image) {
    return ((SignRecognition *) thiz)->recognize(*(Mat *) image, 0);
}
#ifdef __cplusplus
}
#endif
