/*
 * Copyright 2017-2020 The "Eye on the road" Project. Author: Chernyshov Yuriy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <jni.h>

#ifndef _Included_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition
#define _Included_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition
#ifdef __cplusplus
extern "C" {
#endif
/*
 * Class:     DetectionBasedTracker
 * Method:    nativeCreateObject
 * Signature: (Ljava/lang/String;F)J
 */
JNIEXPORT jlong JNICALL Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeCreateObject
        (JNIEnv *, jclass);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeDestroyObject
 * Signature: (J)V
 */
JNIEXPORT void JNICALL Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeDestroyObject
        (JNIEnv *, jclass, jlong);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeSetObjectSize
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeAdd
        (JNIEnv *, jclass, jlong, jstring, jint);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeSetObjectSize
 * Signature: (JI)V
 */
JNIEXPORT void JNICALL Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeInit
        (JNIEnv *, jclass, jlong);

/*
 * Class:     DetectionBasedTracker
 * Method:    nativeDetect
 * Signature: (JJJ)V
 */
JNIEXPORT jint JNICALL Java_chernyshov_yuriy_driverassistant_core_recognition_SignRecognition_nativeRecognize
        (JNIEnv *, jclass, jlong, jlong);

#ifdef __cplusplus
}
#endif
#endif
