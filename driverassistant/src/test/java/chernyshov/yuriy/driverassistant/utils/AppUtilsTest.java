package chernyshov.yuriy.driverassistant.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by Yuriy Chernyshov
 * At Android Studio
 * On 2/15/16
 * E-Mail: chernyshov.yuriy@gmail.com
 */
public final class AppUtilsTest {

    @Test
    public void testSpeedConverter() {
        assertThat(AppUtils.speedConverter(5), is("18.0"));
        assertThat(AppUtils.speedConverter(3.35f), is("12.1"));
        assertThat(AppUtils.speedConverter(16.6667f), is("60.0"));
        assertThat(AppUtils.speedConverter(13.3333f), is("48.0"));
        assertThat(AppUtils.speedConverter(0.277778f), is("1.0"));
        assertThat(AppUtils.speedConverter(28), is("100.8"));
        assertThat(AppUtils.speedConverter(22.19444f), is("79.9"));
    }
}
